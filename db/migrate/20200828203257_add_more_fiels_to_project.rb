class AddMoreFielsToProject < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :progres, :jsonb
    add_column :projects, :porcentage, :integer
    add_column :projects, :project_name, :string
    add_column :projects, :direccion, :date
  end
end
