class AddFechaToProject < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :fecha, :date
  end
end
