class AddFieldsToProject < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :by_pname, :string
    add_column :projects, :by_prop_name, :string
    add_column :projects, :by_cedula, :string
    add_column :projects, :by_mu_name, :string
    add_column :projects, :by_prov_name, :string
    add_column :projects, :by_rnc, :string
  end
end
