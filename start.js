module.exports = {
  apps : [{
    "name"       : "bash-worker",
    "script"     : "./start.sh",
    "exec_interpreter": "bash",
    "exec_mode"  : "fork_mode"
  }]
};