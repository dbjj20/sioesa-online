Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  constraints format: :json  do
    post '/project/:id', to: 'home#get_pro'
    post '/add/project', to: 'home#add_pro'
    post '/update/project/:id', to: 'home#update_pro'
    post '/all/projects/:limit/:offset', to: 'home#get_all'
    post '/search/:key/:value', to: 'home#search'
  end

end
