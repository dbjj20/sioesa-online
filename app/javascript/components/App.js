import React from 'react'
import Main from './src/pages/components/main/index'
import setup from './src/pages/lib/db'
(async ()=>{
  await setup()
})()
export default class App extends React.Component {

  render(){
    return (
      <div>
        <header className="header is-primary">
            <h1 className="title is-size-3 has-text-grey">
                <span><i className="fas fa-archway" aria-hidden="true"/> Sioesa</span>
            </h1>
        </header>
        <Main/>
      </div>
    )
  }
}
