// this will be like the state of react but in node xD.
// simplejs, and inmutable structure
// applay this https://stackoverflow.com/questions/49104247/in-redux-where-does-the-state-actually-get-stored/49104335#:~:text=2%20Answers&text=The%20state%20in%20Redux%20is,that%20state%20gets%20wiped%20out.&text=The%20state%20in%20redux%20is%20just%20a%20variable%20that%20persists,closure)%20by%20all%20redux%20functions.
import React from 'react'
let store = {}

async function addToStore(obj){
  try {
    await Object.assign(store, obj)
    console.log('adding to store', obj)
    return true
  } catch (e) {
    throw e.message
  }
}

async function getStore(callback) {
  try {
    let p = {}
    await Object.assign(p, store)
    console.log('getting store', p)
    await callback(p)
  } catch (e) {
    throw e.message
  }
}

 export {
  addToStore,
  getStore
}
