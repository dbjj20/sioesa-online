import React from 'react'
import Dexie from 'dexie'
import moment from 'moment'
// const fs = window.require('fs')
moment.locale('es-do')

function prepareProperties(obj, db_state, keys){
  let result = {}

    Object.keys(db_state).map((k, i) => {
      keys.map((f) => {
        if(keys){
          if (f === k){
            let v = {}
            Object.keys(db_state[k]).map((d, index)=> {
              Object.assign(v, {[d]: obj[d]})
            })
            Object.assign(result, {[k]: v})
          }
        }
      })
    })
    console.log(result)
  //este metodo crea el mismo objeto que la db necesita para guardar la data y excluye los que no necesita
  // los que vengan del state del componente pueden ser cualquiera y este metodo actuara como filtro y solo pondra los que esten en el esquema de la db
  return result //here goes the objectmodule
}

function getPreparedProperties(keys, obj){
  let result = {}

  if(keys){
    keys.map(o => Object.assign(result, obj[o]))
  }

  return result
}

function calculateStep5(array){
  let _mean = mean(array)
  let _median = median(array)
  let variance = stdeva(array)
  let coef = (variance /_mean) * 100
  if (isNaN(coef)) coef = 0
  console.log('COEF------>', coef)
  return {
    promedio: _mean,
    mediana: _median,
    variacion: variance,
    coeficienteV: coef.toFixed(2)
  }
}

function stdeva(arr){
  console.log('ARRRRRRRR', arr)
  if(arr[0]){
    let array = []
    arr.map((v) => {
      array.push(parseFloat(v.data.vuh))
    })
    console.log("STDEVA ====> ", array)
    const n = array.length;
    const mean = array.reduce((a,b) => a + b)/n.toFixed(2);
    const s = Math.sqrt(array.map(x => Math.pow(x-mean,2)).reduce((a,b) => a+b)/(n-1));
    console.log("JLNASLJBNAS ====>",s)
    if (isNaN(s)) return 0
    return s.toFixed(2)
  }
}

function mean(array){
  console.log("CALCULATE MEAN =>>>>>", array)
  //promedio
  let promedio = []
  let promedio_sum = 0
  if (!array[0]) return '0'

  array.map((a, i) => {
    promedio.push(a.data.vuh)
    promedio_sum = promedio_sum + parseFloat(a.data.vuh)
    console.log(a.data.vuh)
  })

  console.log(promedio_sum / promedio.length)
  let re = promedio_sum / promedio.length
  if (isNaN(re)) return 0
  return re.toFixed(2)
}


function numbers(arr, base = false, i = 0, len, proximity = 1, counter = 0) {
  let array = []
  Object.assign(array, arr)
  if (array.length === 1){
    return array
  }
  if (counter === proximity){
    base = true
  }

  if ( base === true ){
    console.log("end of program")
    console.log("numbers of steps =", proximity)
    // return an ordered array of numbers
    console.log('ORDERED VALUES IN NUMBERS --->', array)
    return array
  }


  /* Start */
  if(!array[0]){
    return
  }

  let i1 = array[i]
  let i2 = array[i + 1]

  if ( i1 > i2 ){
    array[i] = i2
    array[i + 1 ] = i1
  }
  len = array.length
  proximity = len ** 2 - len - 1
  /* - end - */

  return numbers(array, base, i === len ? i = 0 : i + 1 , len, proximity, counter + 1)
  /* xDstriker */
}

function median(array){
  console.log("CALCULATE MEDIAN =>>>>>", array)
  //la media
  let median = []
  let promedio_sum = 0
  if (!array[0]) return '0'
  array.map((a, i) => {
    median.push(parseFloat(a.data.vuh))
  })

  function isEven(n) {
    //par
    //stackoverflow
     return n % 2 == 0;
  }
  console.log('MEDIAN ----->', median)
  let ordered = numbers(median)
  let result
  console.log('ORDERED ----->', ordered)
  if(!ordered) return
  console.log(ordered)
  if (isEven(ordered.length)){
    let mid = ordered.length / 2

    let one = ordered[mid - 1]
    let two = ordered[mid]
    let sum = one + two

    result = sum / 2
  }else {
    let mid = ordered.length / 2
    result = ordered[Math.floor(mid)]
  }
  if (isNaN(result)) return 0
  return result.toFixed(2)
}

class Base {
  constructor(db_table){
    this.instance = db_table // ex: db.tableName....etc....
  }

  async add(values){
    //this should add the values to db
    let date = moment().format()
    try {
      values.created_at = date
      values.updated_at = date
      console.log('ADD =----> ', values)
      await this.instance.add(values)
    } catch (e) {
      console.log('ADD ERROR =----> ', e.message)
    }
  }

  async _get(){
    try {
      return await this.instance.each().toArray()
    } catch (e) {
        console.log('_GET ERROR =----> ', e.message)
    }
  }

  async _getWhere(query){
    try {
      if (!query){
        return 'no query string || object suplied'
      }
      return await this.instance.where(query).toArray()
    } catch (e) {
        console.log('_GET ERROR =----> ', e.message)
    }
  }

  async update(values, db, query){
    try {
      if (values && db && query){
        let data = undefined
        db.transaction("rw", this.instance, async () => {
          data = await this.instance.where(query).toArray()
          console.log('PARAMETERS - - - - -- ', values, query, data)
          // debugger
          if (data[0]){
            values.updated_at = moment().format()
            await this.instance.where(query).modify(values)
            console.log('UPDATED SUCCESS =----> ', values)
          } else {
            console.log('ADD SUCCESS =----> ', values)
            await this.instance.add(values)
          }
        }).catch (Dexie.ModifyError, error => {
            // ModifyError did occur
            console.error(error.failures.length + " items failed to modify")
        })
        // return data
      }
      return 'this method needs values, db, where, query'
    } catch (e) {
        console.log('UPDATE ERROR =----> ', e.message)
    }
  }

  async delete(query){
    try {
      if (query){
        return await this.instance.where(query).delete()
      }
      return 'no query provided'
    } catch (e) {
        console.log('DELETE ERROR =----> ', e.message)
    }
  }

}

function b64toFile(b64Data, filename, contentType) {
    let sliceSize = 512;
    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);

        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        let byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    let file = new File(byteArrays, filename, {type: contentType});
    return file;
}

function saveImg(data, path) {
  let block = data.split(";");
  // Get the content type of the image
  let contentType = block[0].split(":")[1];// In this case "image/gif"
  // get the real base64 content of the file
  let realData = block[1].split(",")[1];
  let tosave = new Buffer(realData, 'base64')
  if (tosave){
    // let path = `${this.props.path}/${this.props.filename}`
    // fs.writeFile(path, tosave, ()=>{})
  }
}

export {
  prepareProperties,
  getPreparedProperties,
  calculateStep5,
  Base,
  b64toFile,
  saveImg
}
