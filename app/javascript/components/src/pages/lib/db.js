import React from 'react'
import Dexie from 'dexie'
import moment from 'moment'
import { addToStore } from './reduxito'
moment.locale('es-do')

const db_app = {
  proyecto: "++id, name, fecha, direccion, project_name, porcentaje, status, by_pname, by_prop_name, data, by_cedula, by_mu_name, by_rnc, by_prov_name, created_at, updated_at, created_by_detail, updated_by_detail",

  descripcion: "++id, proyecto_id, nombre, validacion, created_at, updated_at, created_by_detail, updated_by_detail",
  validacion: "++id, proyecto_id, determinacion, informe_es_valido, presentado",
  descripcion_legal: "++id, proyecto_id, fecha_informe, fecha_inspeccion, tipo_de_inmueble, nombre, cedula, telefono, email, propietario, nombre_propietario, cedula_propietario, telefono_propietario, email_propietario, requerido_por_institucion, requerido_por_rnc, requerido_por_sucursal, requerido_por_gerente, requerido_por_telefono, requerido_por_email, libro, folio, matricula_select_label, matricula_select, designacion_castral, municipio_no, municipio_name, provincia_no, provincia_name, att, *libro_folios, att_count, created_at, updated_at, created_by_detail, updated_by_detail",
  descripcion_inmueble: "++id, proyecto_id, direccion_del_inmueble, coordenadas_inmueble, zoom, latitud_norte, longitud_oeste, created_at, updated_at, created_by_detail, updated_by_detail",
  colindancias: "++id, proyecto_id, norte, este, sur, oeste, created_at, updated_at, created_by_detail, updated_by_detail",
  sector_inmueble: "++id, proyecto_id, locacion, uso_de_suelo, porcentaje_de_solares_edificados, tipo_de_edificacion, clase_social, factores_favorables, factores_desfavorables, crecimiento, conservacion, cambio_de_uso, edad_aparente, demanda_oferta, deseabilidad, otros, created_at, updated_at, created_by_detail, updated_by_detail",
  servicios_disponibles: "++id, proyecto_id, asfaltada, aceras, contenes, area_verde, area_deportiva, dist_a_la_via_principal, drenaje_sanitario, drenaje_pluvial, agua_potable, alumbrado_publico, linea_telefono, cable, est_alta_tension_bco_transformadores, transporte_publico, centros_salud, centros_educativos, of_publicas, comercios_menores, est_gasolina, est_glp, created_at, updated_at, created_by_detail, updated_by_detail",
  vias_acceso: "++id, proyecto_id, principales, secundarias, lugares_sectores_comunidades_cercanas, created_at, updated_at, created_by_detail, updated_by_detail",
  caracteristicas: "++id, proyecto_id, area_const_primer_nivel, area_const_primer_nivel_count, long_frente, long_frente_count, long_fondo_prom, long_fondo_prom_count, aocp, aocp_count, const_solar, factor_frente_fondo, geometria, topografia, ubicacion_manzana, uso_actual, deseabilidad, mejor_mas_alto_uso, *notas_extras, created_at, updated_at, created_by_detail, updated_by_detail",
  otros: "++id, proyecto_id, tipo_de_suelo, nota, created_at, updated_at, created_by_detail, updated_by_detail",
  ofertas_ventas_comparables: "++id, proyecto_id, *data, area_total, promedio, mediana, variacion, coeficienteV, valorTerreno, created_at, updated_at, created_by_detail, updated_by_detail",
  homogenizacion_comparables: "++id, proyecto_id, valor, moneda, area, area_count, vu, vu_count, especulacion, por_dif_area, ubicacion, factor, tipo_de_suelo, topografia, forma_geometrica, factor_homogenizacion, vuh, vuh_count, created_at, updated_at, created_by_detail, updated_by_detail",
  resultados_calculos: "++id, proyecto_id, name, *data, created_at, updated_at, created_by_detail, updated_by_detail",


  caracteristicas_edificacion_mejora: "++id, proyecto_id, cantidad_niveles_pisos, tipo_arquitectura,estado_conservacion,calidad_materiales,deseabilidad_inmueble,acorde_con_entorno,habilitada,edad_aparente,vida_util_estimada,coeficiente,porciento_de_vida,edad_util_remanente,distribucion_x_uno,distribucion_x_dos,distribucion_y_uno,distribucion_y_dos,construccion,terminacion,cantidad,cantidad_count,vup,vup_value,porcentaje_drh,vu_dep,stotal_one,descripcion,porcentaje_dep_fisica,stotal_two,valor_total_x,otros_ajustes_x,porcentaje_x,valor_total_ajustado_x,valor_total_a,otros_ajustes_a,porcentaje_a,valor_total_ajustado_a,valor_total_b,otros_ajustes_b,porcentaje_b,valor_total_ajustado_b, vup_two, vup_value_two, *otras_mejoras_data, valor_en_texto, valor_total_ajustado_input, *distribucion",


  resumen_valores_terrenos: "++id, proyecto_id, valor_total, otros_ajustes, porcentaje, valor_total_ajustado",
  resumen_valores_edificaciones: "++id, proyecto_id, valor_total, otros_ajustes, porcentaje, valor_total_ajustado",
  resumen_valores_otras_mejoras: "++id, proyecto_id, valor_total, otros_ajustes, porcentaje, valor_total_ajustado",
  valores: "++id, proyecto_id, valor_total_inmueble, valor_total_ajustado, valor_en_texto",
  images: "++id, proyecto_id, *data, principal, created_at, updated_at",
  locaciones: "++id, proyecto_id, latitud_norte, latitud_oeste",
  documentos: '++id, proyecto_id, *data, created_at, updated_at',
  pages: '++id, proyecto_id, page_one, page_two, page_three, created_at, updated_at',
  init: "++id, backup_path, created_at, updated_at"
}

const db_state = {
  proyecto: {
    id: Number,
    project_name: '',
    status: '',
    by_pname: '',
    by_prop_name: '',
    by_cedula: '',
    by_mu_name: '',
    by_rnc: '',
    by_prov_name: '',
    data: {},
    porcentaje: Number
  },
  pages: {
    proyecto_id: Number,
    page_one: {},
    page_two: {},
    page_three: {},
  },
  init: {
    id: Number,
    backup_path: ''
  },
  documentos: {
    proyecto_id: Number,
    data: []
  },
  validacion: {
    proyecto_id: Number,
    determinacion: '',
    informe_es_valido: '',
    presentado: ''
  },
  descripcion_legal: {
    proyecto_id: Number,
    fecha_informe: '',
    fecha_inspeccion: '',
    tipo_de_inmueble: '',
    nombre: '',
    cedula: '',
    telefono: '',
    email: '',
    propietario: '',
    nombre_propietario: '',
    cedula_propietario: '',
    telefono_propietario: '',
    email_propietario: '',
    requerido_por_institucion: '',
    requerido_por_rnc: '',
    requerido_por_sucursal: '',
    requerido_por_gerente: '',
    requerido_por_telefono: '',
    requerido_por_email: '',
    libro: '',
    folio: '',
    matricula_select_label: '',
    matricula_select: '',
    designacion_castral: '',
    municipio_no: '',
    municipio_name: '',
    provincia_no: '',
    provincia_name: '',
    att: '',
    att_count: '',
    libro_folios: [],
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  descripcion_inmueble: {
    proyecto_id: Number,
    direccion_del_inmueble: '',
    coordenadas_inmueble: '',
    latitud_norte: '',
    longitud_oeste: '',
    zoom: 11,
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  colindancias: {
    proyecto_id: Number,
    norte: '',
    este: '',
    sur: '',
    oeste: '',
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  sector_inmueble: {
    proyecto_id: Number,
    locacion: '',
    uso_de_suelo: '',
    porcentaje_de_solares_edificados: '',
    tipo_de_edificacion: '',
    clase_social: '',
    factores_favorables: '',
    factores_desfavorables: '',
    crecimiento: '',
    conservacion: '',
    cambio_de_uso: '',
    edad_aparente: '',
    demanda_oferta: '',
    deseabilidad: '',
    otros: '',
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  servicios_disponibles: {
    proyecto_id: Number,
    asfaltada: '',
    aceras: '',
    contenes: '',
    area_verde: '',
    area_deportiva: '',
    dist_a_la_via_principal: '',
    drenaje_sanitario: '',
    drenaje_pluvial: '',
    agua_potable: '',
    alumbrado_publico: '',
    linea_telefono: '',
    cable: '',
    est_alta_tension_bco_transformadores: '',
    transporte_publico: '',
    centros_salud: '',
    centros_educativos: '',
    of_publicas: '',
    comercios_menores: '',
    est_gasolina: '',
    est_glp: '',
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  vias_acceso: {
    proyecto_id: Number,
    principales: '',
    secundarias: '',
    lugares_sectores_comunidades_cercanas: '',
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  caracteristicas: {
    proyecto_id: Number,
    area_const_primer_nivel: '',
    area_const_primer_nivel_count: '',
    long_frente: '',
    long_frente_count: '',
    long_fondo_prom: '',
    long_fondo_prom_count: '',
    aocp: '',
    aocp_count: '',
    const_solar: '',
    factor_frente_fondo: '',
    geometria: '',
    topografia: '',
    ubicacion_manzana: '',
    uso_actual: '',
    deseabilidad: '',
    mejor_mas_alto_uso: '',
    notas_extras: Array,
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  otros: {
    proyecto_id: Number,
    tipo_de_suelo: '',
    nota: '',
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  ofertas_ventas_comparables: {
    proyecto_id: Number,
    data: Array,
    promedio: 0.00,
    mediana: 0.00,
    variacion: 0.00,
    coeficienteV: 0.00,
    valorTerreno: 0.00,
    area_total:0.00,
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  homogenizacion_comparables: {
    proyecto_id: Number,
    valor: '',
    moneda: '',
    area: '',
    area_count: '',
    vu: '',
    vu_count: '',
    especulacion: '',
    por_dif_area: '',
    ubicacion: '',
    factor: '',
    tipo_de_suelo: '',
    topografia: '',
    forma_geometrica: '',
    factor_homogenizacion: '',
    vuh: '',
    vuh_count: '',
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  resultados_calculos: {
    proyecto_id: Number,
    name: '',
    data: Array,
    created_at: '',
    updated_at: '',
    created_by_detail: '',
    updated_by_detail: ''
  },
  caracteristicas_edificacion_mejora: {
    proyecto_id: Number,
    cantidad_niveles_pisos: undefined,
    tipo_arquitectura: undefined,
    estado_conservacion: undefined,
    calidad_materiales: undefined,
    deseabilidad_inmueble: undefined,
    acorde_con_entorno: undefined,
    habilitada: undefined,
    edad_aparente: undefined,
    vida_util_estimada: undefined,
    coeficiente: undefined,
    porciento_de_vida: undefined,
    edad_util_remanente: undefined,
    distribucion_x_uno: undefined,
    distribucion_x_dos: undefined,
    distribucion_y_uno: undefined,
    distribucion_y_dos: undefined,
    construccion: undefined,
    terminacion: undefined,
    cantidad: undefined,
    cantidad_count: undefined,
    vup: undefined,
    vup_value: undefined,
    porcentaje_drh: undefined,
    vu_dep: undefined,
    stotal_one: undefined,
    descripcion: undefined,
    porcentaje_dep_fisica: undefined,
    stotal_two: undefined,

    valor_total_x: undefined,
    otros_ajustes_x: undefined,
    porcentaje_x: undefined,
    valor_total_ajustado_x: undefined,

    valor_total_a: undefined,
    otros_ajustes_a: undefined,
    porcentaje_a: undefined,
    valor_total_ajustado_a: undefined,

    valor_total_b: undefined,
    otros_ajustes_b: undefined,
    porcentaje_b: undefined,
    valor_total_ajustado_b: undefined,
    otras_mejoras_data: [],
    valor_en_texto: undefined,
    valor_total_ajustado_input: undefined,
    distribucion: []
  },

  valores: {
    proyecto_id: Number,
    valor_total_inmueble: undefined,
    valor_total_ajustado: undefined,
    valor_en_texto: undefined
  },
  images: {
    proyecto_id: Number,
    data: [],
    principal: ''
  },
  locaciones: {
    proyecto_id: Number,
    latitud_norte: undefined,
    latitud_oeste: undefined
  },
}

const db_users = {
    user: '++id, fullname, email, created_at, updated_at'
}

async function createdb(name){
  if (name){
    console.log(name)
    let db = new Dexie(String(name));
    db.version(1).stores(db_app)
    //si la db esta creada solo retornara la db sin crearla
    return db
  } else {
    console.log('no Name in create db')
  }
}

async function init(){
  //return all db names stored
  let dbs = await Dexie.getDatabaseNames()
  let a = []
  let db = await createdb('Sioesa_data') //if exist it just will return db object

  if(dbs){
    dbs.forEach((item, i) => {
      a.push(item)
    })
  }

  if (!a[0]){
    //si la db no esta creada la creara, si hay no hara nada
    try {
      db.init.add({
        created_at: moment().format()
      })
    } catch (e) {
      console.log(e.message)
    }
  }

  return {a, db}
}

function arrayCeroToObject(arr){
  let o = arr
  return o[0]
}

async function getAllModelsDataByProjectId(id, store){
  console.log('IN getAllModelsDataByProjectId ---->', id)
  console.log('IN getAllModelsDataByProjectId STORE ---->', store)
  let db_data = await dataFetch({}, `/project/${id}`, 'post')

  return db_data.data.data
  //aqui carga toda la data del paso 10
  // await addToStore({db_data})
}

async function dataFetch(data, url, method = 'post'){
    let response = await fetch(url, {
        method: method,
        credentials: "same-origin",
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
        },
        ...method === 'get' ? {}:{body: JSON.stringify(data)}
      })
    return await response.json()
  }
// (async () => { })()

export default async function (){
  await addToStore({
    db_state: db_state,
    init: init,
    createdb: createdb,
    getAllModelsDataByProjectId: getAllModelsDataByProjectId,
    dataFetch: dataFetch,
    rails_data:[]
  })
}
