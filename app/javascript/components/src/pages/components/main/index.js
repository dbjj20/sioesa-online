import React from 'react'
import FirstView from '../comp/firstView'
import MapView from '../comp/mapView'
import BackUpView from '../comp/backupView'
import ConfigView from '../comp/configView'

export default class Main extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      inView: 'Tasaciones'
    }
    this.changeView = this.changeView.bind(this)
    this.cancel = this.cancel.bind(this)
  }
  async cancel(){
    await this.setState({inView: ''})
    await this.setState({inView: 'Tasaciones'})
  }

  async changeView(v){
    await this.setState({
      inView: v
    })
  }

  render(){
    return(
        <div className="grid_layout">
            <aside className="side_menu has-background-dark menu">
                <ul className="menu-list">
                    <li>
                        <a className={this.state.inView === "Tasaciones" ? "is-active": ""} onClick={() => this.changeView('Tasaciones')}>
                            <span className="icon is-medium">
                                <i className="fas fa-home" aria-hidden="true"></i>
                            </span>
                            Tasaciones
                        </a>
                    </li>
                    <li>
                        <a className={this.state.inView === "Backup" ? "is-active": ""} onClick={() => this.changeView('Backup')}>
                            <span className="icon is-medium">
                                <i className="fas fa-save" aria-hidden="true"></i>
                            </span>
                            Backup
                        </a>
                    </li>
                </ul>
            </aside>
            <section className="section main_container">
              {this.state.inView === "Tasaciones" ? (
                <FirstView cancel={()=> this.cancel()}/>
              ):[]}

              {this.state.inView === "Backup" ? (
                <BackUpView/>
              ):[]}

            </section>
        </div>
    )
  }
}
