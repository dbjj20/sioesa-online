import React from 'react'

export default class Step6Module extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      cantidad: '',
      cantidad_select: '',
      vup: '',
      vup_value: '',
      porcentaje_dep_fisica:'',
      vu_dep: '',
      stotal: '',
      descripcion: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.save = this.save.bind(this)
    this.vu_dep = this.vu_dep.bind(this)
    this.stotal = this.stotal.bind(this)
  }

  stotal(){
    let result = parseFloat(this.vu_dep() * this.state.cantidad)
    return result.toFixed(2)
  }

  vu_dep(){
    let result = parseFloat(this.state.vup_value - (this.state.vup_value *  this.state.porcentaje_dep_fisica / 100))
    return result.toFixed(2)
  }

  async save(){
    await this.setState({
      vu_dep: this.vu_dep(),
      stotal: this.stotal()
    })
    await this.props.add({
      id: this.props.id,
      state: this.state
    })
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    console.log("DATA FROM PROPS MODULE STEP 6 =---->", this.props.data)
    await this.setState(this.props.data)
  }

  render(){
    return (
      <div>
      <div className="field">
        <label className="label is-small">Descripción</label>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded">
              <input name="descripcion" value={this.state.descripcion} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
            </p>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <div className="field">
            <label className="label is-small">Cant.</label>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control is-expanded">
                  <input name="cantidad" value={this.state.cantidad} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                </p>
                <p className="control">
                  <span className="select is-small">
                    <select name="cantidad_select" value={this.state.cantidad_select} onChange={this.handleChange}>
                      <option>PA</option>
                      <option>m</option>
                    </select>
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="column">
          <div className="field">
            <label className="label is-small">VUP</label>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <span className="select is-small">
                    <select name="vup" value={this.state.vup} onChange={this.handleChange}>
                      <option>RD$</option>
                      <option>US$</option>
                    </select>
                  </span>
                </p>
                <p className="control is-expanded">
                  <input name="vup_value" value={this.state.vup_value} onChange={this.handleChange} className="input is-small" type="text" placeholder="" />
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="column">
          <div className="field">
            <label className="label is-small has-text-danger">(%) Dep. física</label>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-small is-static">
                    %
                  </a>
                </p>
                <p className="control is-expanded">
                  <input name="porcentaje_dep_fisica" value={this.state.porcentaje_dep_fisica} onChange={this.handleChange} className="input is-small" type="text" placeholder="" />
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="column">
          <div className="field">
            <label className="label is-small">V. U. Dep</label>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-small is-static">
                    RD$
                  </a>
                </p>
                <p className="control is-expanded">
                  <input name="vu_dep" value={this.vu_dep()} className="input is-small" type="text" placeholder="" />
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="column">
          <div className="field">
            <label className="label is-small">S-Total</label>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-small is-static">
                    RD$
                  </a>
                </p>
                <p className="control is-expanded">
                  <input name="stotal" value={this.stotal()} className="input is-small" type="text" placeholder="" />
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="field is-grouped">
        <p className="control">
          <a className="button is-small is-danger" onClick={() => this.props.removeElement(this.props.id)}>
            <i className="fas fa-minus"/>
          </a>
          &nbsp;
          <a className="button is-small is-info" onClick={this.save}>
            <i className="fas fa-save"/>
          </a>
        </p>
      </div>
      <div className="divider"/>
      </div>
    )
  }
}
