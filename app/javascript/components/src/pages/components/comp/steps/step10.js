import React from 'react'
import { getPreparedProperties, calculateStep5 } from '../../../lib/utils'
import moment from 'moment'
const date_format = "dddd D [de] MMMM yyyy"

export default class Step10 extends React.Component{
	constructor(props){
		super(props)
		
		this.state = {
			acorde_con_entorno: "",
			area_total: "",
			att: "",
			att_count: "",
			calidad_materiales: "",
			cantidad: "",
			cantidad_count: "",
			cantidad_niveles_pisos: "",
			cedula: "",
			cedula_propietario: "",
			coeficiente: "",
			coeficienteV: "",
			construccion: "",
			coordenadas_inmueble: "",
			created_at: "",
			created_by_detail: undefined,
			data: [],
			descripcion: "",
			deseabilidad_inmueble: "",
			designacion_castral: "",
			determinacion: "",
			direccion_del_inmueble: "",
			distribucion_x_dos: "",
			distribucion_x_uno: "",
			distribucion_y_dos: "",
			distribucion_y_uno: "",
			edad_aparente: "",
			edad_util_remanente: "",
			email: "",
			email_propietario: "",
			estado_conservacion: "",
			este: "",
			fecha_informe: "",
			fecha_inspeccion: "",
			folio: "",
			habilitada: "",
			id: 0,
			informe_es_valido: "",
			latitud_norte: "",
			libro: "",
			longitud_oeste: "",
			matricula_select: "",
			matricula_select_label: "",
			mediana: "",
			municipio_name: "",
			municipio_no: "",
			nombre: "",
			nombre_propietario: "",
			norte: "",
			oeste: "",
			otras_mejoras_data: [],
			otros_ajustes_a: "",
			otros_ajustes_b: "",
			otros_ajustes_x: "",
			porcentaje_a: "",
			porcentaje_b: "",
			porcentaje_dep_fisica: "",
			porcentaje_drh: "",
			porcentaje_x: "",
			porciento_de_vida: 0,
			presentado: "",
			promedio: "",
			propietario: "",
			provincia_name: "",
			provincia_no: "",
			proyecto_id: 0,
			requerido_por_email: "",
			requerido_por_gerente: "",
			requerido_por_institucion: "",
			requerido_por_rnc: "",
			requerido_por_sucursal: "",
			requerido_por_telefono: "",
			stotal_one: "",
			stotal_two: "",
			sur: "",
			telefono: "",
			telefono_propietario: "",
			terminacion: "",
			tipo_arquitectura: "",
			tipo_de_inmueble: "",
			updated_at: "",
			updated_by_detail: undefined,
			valorTerreno: 0,
			valor_en_texto: "",
			valor_total_a: "",
			valor_total_ajustado_a: "",
			valor_total_ajustado_b: "",
			valor_total_ajustado_input: "",
			valor_total_ajustado_x: "",
			valor_total_b: "",
			valor_total_x: "",
			variacion: "",
			vida_util_estimada: "",
			vu_dep: "",
			vup: "",
			vup_value: "",
			distribucion: [],
			libro_folios: [],
			imagenes: {data: []},
			documentos: {data: []},
			page_one: {},
			page_two: {},
			page_three: {}
		}
		this.capitalize = this.capitalize.bind(this)
		this.capitalizeMoment = this.capitalizeMoment.bind(this)
		this.factorHomogenizacion = this.factorHomogenizacion.bind(this)
		this.vuh = this.vuh.bind(this)
		this.vu_dep = this.vu_dep.bind(this)
		this.stotal = this.stotal.bind(this)
		this.otras_mejoras_data_stotal = this.otras_mejoras_data_stotal.bind(this)
		this.otras_mejoras_data_vu_dep = this.otras_mejoras_data_vu_dep.bind(this)
		
		this.valor_total_ajustado_terrenos = this.valor_total_ajustado_terrenos.bind(this)
		this.valor_total_ajustado_a = this.valor_total_ajustado_a.bind(this)
		this.valor_total_ajustado_x = this.valor_total_ajustado_x.bind(this)
		this.valor_total_ajustado_b = this.valor_total_ajustado_b.bind(this)
		this.valor_total = this.valor_total.bind(this)
		this.valor_total_inmueble = this.valor_total_inmueble.bind(this)
	}
	
	valor_total_inmueble(){
		let result = parseFloat(this.valor_total_ajustado_x()) + parseFloat(this.valor_total_ajustado_b()) + parseFloat(this.valor_total_ajustado_a())
		return result.toFixed(2)
	}
	
	valor_total(){
		let s = 0
		let sum = this.state.otras_mejoras_data[0] ? this.state.otras_mejoras_data.map((d, i) => {
			// debugger
			s = s + parseFloat(d.data.stotal)
		}) : ''
		
		return s
	}
	
	valor_total_ajustado_x(){
		let result = parseFloat(this.valor_total() - (this.valor_total() * this.state.porcentaje_x) / 100)
		return result.toFixed(2)
	}
	
	valor_total_ajustado_b(){
		let result = parseFloat(this.stotal() - (this.stotal() * this.state.porcentaje_b) / 100)
		return result.toFixed(2)
	}
	
	valor_total_ajustado_a(){
		let result = parseFloat(this.valor_total_ajustado_terrenos() - (this.valor_total_ajustado_terrenos() * this.state.porcentaje_a) / 100)
		return result.toFixed(2)
	}
	
	valor_total_ajustado_terrenos(){
		let result = parseFloat(this.state.area_total * this.state.mediana)
		return result.toFixed(2)
	}
	
	otras_mejoras_data_stotal(cantidad, vup_value, porcentaje_dep_fisica){
		let result = parseFloat(this.otras_mejoras_data_vu_dep(vup_value, porcentaje_dep_fisica) * cantidad)
		return result.toFixed(2)
	}
	
	otras_mejoras_data_vu_dep(vup_value, porcentaje_dep_fisica){
		let result = parseFloat(vup_value - (vup_value * porcentaje_dep_fisica / 100))
		return result.toFixed(2)
	}
	
	stotal(){
		let result = parseFloat(this.vu_dep(6) * this.state.cantidad)
		return result.toFixed(2)
	}
	
	vu_dep(fix){
		let result = parseFloat(parseFloat(this.state.vup_value).toFixed(6) - (parseFloat(this.state.vup_value).toFixed(6) * (this.state.porcentaje_drh)) / 100)
		return result.toFixed(fix || 2)
	}
	
	vuh(vu, data){
		let factor_homogenizacion = this.factorHomogenizacion(data)
		let result = vu * factor_homogenizacion
		
		return result.toFixed(2)
	}
	
	factorHomogenizacion(data){
		let {especulacion, ubicacion, factor, tipo_de_suelo, topografia, forma_geometrica, area} = data
		let por_dif_area = String(parseFloat((area / parseFloat(this.state.area_total * this.state.mediana)) ** 0.1).toFixed(4))
		let result = parseFloat(especulacion) * parseFloat(ubicacion) * parseFloat(factor) * parseFloat(tipo_de_suelo) * parseFloat(topografia) * parseFloat(forma_geometrica) * parseFloat(por_dif_area)
		// await this.setState({factor_homogenizacion: result.toFixed(4)})
		return result.toFixed(4)
	}
	
	capitalize(str){
		return String(str).replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase())))
	}
	
	capitalizeMoment(str){
		return String(moment(new Date(str)).format(date_format)).replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase())))
	}
	
	async componentDidMount(){
		if (this.props.db_data){
			let data = await this.props.db_data()
			console.log('STEP 10 PRINT ----->', data)
			
			await this.setState(
				getPreparedProperties([
					'descripcion',
					'validacion',
					'descripcion_legal',
					'descripcion_inmueble',
					'colindancias',
					'sector_inmueble',
					'servicios_disponibles',
					'vias_acceso',
					'caracteristicas',
					'ofertas_ventas_comparables',
					'homogenizacion_comparables',
					'resultados_calculos',
					'caracteristicas_edificacion_mejora',
					'pages'
				], data))
			
			await this.setState(calculateStep5(this.state.data))
			await this.setState({
				imagenes: getPreparedProperties(['images'], data),
				documentos: getPreparedProperties(['documentos'], data)
			})
			let p = getPreparedProperties(['proyecto'], data)
			p.p_data = p.data
			delete p.data
			await this.setState(p)
			
			console.log(this.state)
		}
	}
	
	render(){
		return(
			<div>
				<div className="container">
					<h2 className="title is-4">
						Resumen
					</h2>
					<div className="divider"></div>
					<div className="result">
						{/*<!-- page 1 start -->*/}
						
						
						{this.state.page_one && this.state.page_one.active ? (
							<>
								<div className="columns is-multiline">
									<div className="column is-one-quarter">
										<figure className="image is-4by3">
											<img src={`file://${this.state.page_one.save_path}`}/>
										</figure>
									</div>
								</div>
							</>
						) : (
							<>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Descripción del inmueble</h2>
									</div>
									<div className="column is-full">
										<p className="has-text-centered is-size-7 has-text-weight-bold">Tasación para la determinación del valor de mercado &nbsp; | &nbsp; Este informe es válido para ser presentado como garantia hipotecaria </p>
									</div>
								</div>
								<div className="columns is-multiline">
									
									<div className="column is-4">
										<div className="columns is-multiline">
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Fecha informe</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.capitalizeMoment(this.state.fecha_informe)}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Fecha inspección</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.capitalizeMoment(this.state.fecha_inspeccion)}</p>
											</div>
										</div>
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Solicitado por</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Nombre</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.capitalize(this.state.nombre)}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Cédula/RNC</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.cedula}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Teléfono</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.telefono}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Email</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.email}</p>
											</div>
										</div>
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Requerido por</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Institución</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.requerido_por_institucion}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">RNC</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.requerido_por_rnc}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Sucursal</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.requerido_por_sucursal}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Gerente</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.requerido_por_gerente}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Teléfono</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.requerido_por_telefono}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Email</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.requerido_por_email}</p>
											</div>
										</div>
									</div>
									<div className="column is-5">
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Propietario</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Nombre</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.capitalize(this.state.nombre_propietario)}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Cédula/RNC</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.cedula_propietario}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Teléfono</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.telefono_propietario}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Email</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.email_propietario}</p>
											</div>
										</div>
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Descripción legal</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Tipo de inmueble</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.tipo_de_inmueble}</p>
											</div>
											
											
											{/*<div className="column is-one-third is-label">
                      <p className="has-text-right is-size-7 has-text-weight-bold">Libro / Folio</p>
                    </div>
                    <div className="column is-two-thirds is-field">
                      <p className="is-size-7">23423234/2342332, 23232253,</p>
                    </div>*/}
											
											{this.state.libro_folios[0] ? this.state.libro_folios.map((a, i) => {
												return(
													<React.Fragment key={i}>
														<div className="column is-one-third is-label">
															<p className="has-text-right is-size-7 has-text-weight-bold">Libro / Folio #{i + 1}</p>
														</div>
														<div className="column is-two-thirds is-field">
															<p className="is-size-7">{`${a.data.libro} / ${a.data.folio}`}</p>
														</div>
													</React.Fragment>
												)
											}):[]}
											
											
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">{this.state.matricula_select_label}</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.matricula_select}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Designación catastral</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.designacion_castral}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Provincia</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{`${this.state.provincia_no} - ${this.state.provincia_name}`}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Municipio</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{`${this.state.municipio_no} - ${this.state.municipio_name}`}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Área total titulada</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{`${this.state.att} ${this.state.att_count}`}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Dirección del inmueble</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.direccion_del_inmueble}</p>
											</div>
										</div>
									</div>
									<div className="column is-3">
										<div className="columns is-multiline">
											<div className="column is-full">
												<figure className="image is-4by3">
													{this.state.imagenes.data ? this.state.imagenes.data.map((d, i)=> {
														if (d.id === this.state.imagenes.principal){
															return (
																<img key={i} src={'file://' + d.data.save_path}/>
															)
														}
													}) : (<img src="https://www.thehousedesigners.com/house-plans/images/AdvSearch2-7263.jpg"/>)}
												</figure>
											</div>
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Colindancias generales del lote</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Norte</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.norte}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Este</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.este}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Sur</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.sur}</p>
											</div>
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Oeste</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.oeste}</p>
											</div>
										</div>
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Coordenadas geográficas</p>
											</div>
											<div className="column is-two-fifths is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Latitud norte</p>
											</div>
											<div className="column is-three-fifths is-field">
												<p className="is-size-7">{this.state.latitud_norte}</p>
											</div>
											<div className="column is-two-fifths is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Longitud Oste</p>
											</div>
											<div className="column is-three-fifths is-field">
												<p className="is-size-7">{this.state.longitud_oeste}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Vecindad y/o sector del inmueble</h2>
										<p className="has-text-centered is-size-7 subtitle">(Factores predominantes)</p>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Locación</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.locacion}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Uso de suelo</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.uso_de_suelo}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Tipo de edificaciones</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.tipo_de_edificacion}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Clase social</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.clase_social}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">% de solares edificados</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.porcentaje_de_solares_edificados}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Crecimiento</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.crecimiento}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Conservación</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.conservacion}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Cambio de uso</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.cambio_de_uso}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Edad aparente</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.edad_aparente}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Demanda / Oferta</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.demanda_oferta}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Deseabilidad</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.deseabilidad}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-2 is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Factores desfavorables</p>
											</div>
											<div className="column is-4 is-field">
												<p className="is-size-7">{this.state.factores_desfavorables}</p>
											</div>
											<div className="column is-2 is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Factores favorables</p>
											</div>
											<div className="column is-4 is-field">
												<p className="is-size-7">{this.state.factores_favorables}</p>
											</div>
											<div className="column is-one-fifth is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Otros</p>
											</div>
											<div className="column is-four-fifths is-field">
												<p className="is-size-7">{this.state.otros}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Servicios disponible en el sector y/o zona</h2>
										<p className="has-text-centered is-size-7 subtitle"> Lejano {">"} 2 km &nbsp; | &nbsp; Cercano {"<"} 2 km y {">"} 1 km  &nbsp;|&nbsp;  Proximo {"<"} 1 km</p>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Vías y áreas públicas</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Asfaltada</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.asfaltada}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Aceras</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.aceras}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Contenes</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.contenes}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Área verde</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.area_verde}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Área Deportivas</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.area_deportiva}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Distancia a la vía principal</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.dist_a_la_via_principal}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Servicios básicos</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Drenaje sanitario</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.drenaje_sanitario}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Drenaje pluvial</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.drenaje_pluvial}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Agua potable</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.agua_potable}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Alumbrado público</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.alumbrado_publico}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Linea de telefono y cable</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.linea_telefono}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Est. de alta tensión y/o Bco. transformadores</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.est_alta_tension_bco_transformadores}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-full">
												<p className="has-text-centered is-size-6">Otros servicios</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Transporte público</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.transporte_publico}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Centros de salud</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.centros_salud}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Centros educativos</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.centros_educativos}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Oficinas Públicas</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.of_publicas}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Comercios menores</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.comercios_menores}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Est. de gasolina</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.est_gasolina}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Est. de GLP</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.est_glp}</p>
											</div>
										</div>
									</div>
									<div className="column is-full">
										<p className="has-text-centered is-size-6">Vías de acceso</p>
									</div>
									<div className="column is-two-fifths">
										<div className="columns is-multiline">
											<div className="column is-one-quarter is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Principales</p>
											</div>
											<div className="column is-three-quarters is-field">
												<p className="is-size-7">{this.state.principales}</p>
											</div>
										</div>
									</div>
									<div className="column is-three-fifths">
										<div className="columns is-multiline">
											<div className="column is-one-quarter is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Secundarias</p>
											</div>
											<div className="column is-three-quarters is-field">
												<p className="is-size-7">{this.state.secundarias}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-one-third is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Lugares, sectores y comunidades cercanas y/o colindantes</p>
											</div>
											<div className="column is-two-thirds is-field">
												<p className="is-size-7">{this.state.lugares_sectores_comunidades_cercanas}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Caracteristicas del emplazamiento o solar</h2>
										<p className="has-text-centered is-size-7 subtitle">Longitudes aproximadas levantadas con cinta y/o laser métrico de mano</p>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Área const. 1er nivel</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{`${this.state.area_const_primer_nivel} ${this.state.area_const_primer_nivel_count}`}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Long. frente</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{`${this.state.long_frente} ${this.state.long_frente_count}`}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Long. fondo prom.</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{`${this.state.long_fondo_prom} ${this.state.long_fondo_prom_count}`}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">(AOCP) = Área ocupada </p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{`${this.state.aocp} ${this.state.aocp_count}`}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">% Const. / solar</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.const_solar}%</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Factor frente / fondo </p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.factor_frente_fondo}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Ubicación manzana</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.ubicacion_manzana}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Geometría</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.geometria}</p>
											</div>
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Topografía</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.topografia}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Uso actual</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.uso_actual}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Mejor y más alto uso</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.mejor_mas_alto_uso}</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Deseabilidad</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.deseabilidad}</p>
											</div>
										</div>
									</div>
									<div className="column is-12">
										<div className="columns is-multiline">
											<div className="column is-one-fifth is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Notas extras</p>
											</div>
											<div className="column is-four-fifths is-field">
												<p className="is-size-7">{this.state.notas_extras}</p>
											</div>
										</div>
									</div>
								</div>
							</>
						)}
						{/*<!-- page 1 end -->*/}
						
						
						
						
						{/*<!-- page 2 start-->*/}
						{this.state.page_two && this.state.page_two.active ? (
							<>
								<div className="columns is-multiline">
									<div className="divider"></div>
									<div className="column is-one-quarter">
										<figure className="image is-4by3">
											<img src={`file://${this.state.page_two.save_path}`}/>
										</figure>
									</div>
								</div>
							</>
						) : (
							<>
								<div className="divider"/>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Análisis del valor comparativa</h2>
										<p className="has-text-centered is-size-7">Letreros expuestos en la via pública, ventas no realizadas, que no establecen un precio en la zona pero, si nos dan un estimado de los valores máximos ofertados por los dueños de las propiedades, que manejados con criterio profesional podemos determinar el valor máximo promedio del inmueble de interes</p>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											{/*
                      <div className="column is-1 is-label">
                        <p className="has-text-right is-size-7 has-text-weight-bold">1</p>
                      </div>
                      <div className="column is-11 is-field">
                        <p className="is-size-7">2018, ESPAÑA, As=187.92 M2., Ac=638.93 M2. por RD $15,000,000.00 (ANGEL ROSARIO AL 809-805-3218)</p>
                      </div>
                      */}
											{this.state.data[0] ? this.state.data.map((a, i) => {
												return (
													<React.Fragment key={i}>
														<div className="column is-1 is-label">
															<p className="has-text-right is-size-7 has-text-weight-bold">{i + 1}</p>
														</div>
														<div key={i} className="column is-11 is-field">
															<p className="is-size-7">{a.value}</p>
														</div>
													</React.Fragment>
												)
											}):[]}
										
										</div>
									</div>
								</div>
								<div className="columns is-multiline is-2 is-variable">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Homogenización de las comparables</h2>
									</div>
								</div>
								
								<table className="table is-narrow">
									<thead>
									<tr className="is-full is-label" style={{background: '#eeeeee'}}>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Valor</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Área</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">V.U.</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Especulación</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Por dif. de área</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Ubicación</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Factor (frente/fondo)</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Tipo de suelo</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold" >Topografía</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Forma geometrica</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">Factor homogenización</th>
										<th className="has-text-centered is-size-7 has-text-weight-bold">V.U.H</th>
									</tr>
									</thead>
									<tbody className="table is-bordered">
									{this.state.data[0] ? this.state.data.map((a, i) => {
										return (
											<tr key={i}>
												<td className="has-text-centered is-size-7">{String(parseFloat(a.data.area * a.data.vu).toFixed(2)).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</td>
												<td className="has-text-centered is-size-7">{a.data.area}</td>
												<td className="has-text-centered is-size-7">{a.data.vu}</td>
												<td className="has-text-centered is-size-7">{a.data.especulacion}</td>
												<td className="has-text-centered is-size-7">{String(parseFloat((a.data.area / parseFloat(this.state.area_total * this.state.mediana)) ** 0.1).toFixed(4))}</td>
												<td className="has-text-centered is-size-7">{a.data.ubicacion}</td>
												<td className="has-text-centered is-size-7">{a.data.factor}</td>
												<td className="has-text-centered is-size-7">{a.data.tipo_de_suelo}</td>
												<td className="has-text-centered is-size-7">{a.data.topografia}</td>
												<td className="has-text-centered is-size-7">{a.data.forma_geometrica}</td>
												<td className="has-text-centered is-size-7">{this.factorHomogenizacion(a.data)}</td>
												<td className="has-text-centered is-size-7">{String(this.vuh(a.data.vu, a.data)).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</td>
											</tr>
										)
									}):[]}
									</tbody>
								</table>
								
								<div className="columns is-multiline is-2 is-variable">
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Promedio (X)</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.promedio}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Mediana (Xmed)</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.mediana}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Desvest. M (S)</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.variacion}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-quarter">
										<div className="columns is-multiline">
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Coef. de variación (CV)</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.coeficienteV}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Valor del terreno</h2>
									</div>
									<div className="column is-one-fifth">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Area total</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">{this.state.area_total} m²</p>
											</div>
										</div>
									</div>
									<div className="column is-two-fifths">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Valor unitario promedio en el sector</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7">RD${String(this.state.mediana).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
									<div className="column is-two-fifths">
										<div className="columns is-multiline">
											<div className="column is-half is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Valor total del terreno</p>
											</div>
											<div className="column is-half is-field">
												<p className="is-size-7 has-text-weight-bold">RD${parseFloat(this.state.area_total * this.state.mediana).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Caracteristica de la edificación y mejoras</h2>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-three-fifths is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Cantidad de niveles/pisos</p>
											</div>
											<div className="column is-two-fifths is-field">
												<p className="is-size-7">{this.state.cantidad_niveles_pisos}</p>
											</div>
											<div className="column is-three-fifths is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Tipo de Arquitectura</p>
											</div>
											<div className="column is-two-fifths is-field">
												<p className="is-size-7">{this.state.tipo_arquitectura}</p>
											</div>
											<div className="column is-three-fifths is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Estado de conservación</p>
											</div>
											<div className="column is-two-fifths is-field">
												<p className="is-size-7">{this.state.estado_conservacion}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-three-quarters is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Calidad de los materiales</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">{this.state.calidad_materiales}</p>
											</div>
											<div className="column is-three-quarters is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Deseabilidad del inmueble</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">{this.state.deseabilidad_inmueble}</p>
											</div>
											<div className="column is-three-quarters is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Acorde con el entorno</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">{this.state.acorde_con_entorno}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-three-quarters is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Habilitada</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">{this.state.habilitada}</p>
											</div>
											<div className="column is-three-quarters is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Coeficiente (K)</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">{this.state.coeficiente}</p>
											</div>
											<div className="column is-three-quarters is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Edad aparente</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">{this.state.edad_aparente} años</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Vida útil estimada</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.vida_util_estimada} años</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Edad útil remanente</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{this.state.edad_util_remanente} años</p>
											</div>
											<div className="column is-two-thirds is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">% de vida</p>
											</div>
											<div className="column is-one-third is-field">
												<p className="is-size-7">{`${String(parseFloat((this.state.vida_util_estimada / this.state.edad_aparente) * 100).toFixed(2))}%`}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Valor de edificación</h2>
									</div>
									<div className="column is-12">
										<div className="columns is-multiline">
											{this.state.distribucion[0] ? this.state.distribucion.map((a, i) => {
												return(
													<React.Fragment key={i}>
														<div className="column is-1 is-label">
															<p className="has-text-right is-size-7 has-text-weight-bold">{i + 1}</p>
														</div>
														<div className="column is-2 is-field">
															<p className="is-size-7">{a.data.key}</p>
														</div>
														<div className="column is-9 is-field">
															<p className="is-size-7">{a.data.value}</p>
														</div>
													</React.Fragment>
												)
											}):[]}
										</div>
									</div>
									<div className="column is-half">
										<div className="columns is-multiline">
											<div className="column is-one-quarter is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Construcción</p>
											</div>
											<div className="column is-three-quarters is-field">
												<p className="is-size-7">{this.state.construccion}</p>
											</div>
										</div>
									</div>
									<div className="column is-half">
										<div className="columns is-multiline">
											<div className="column is-one-quarter is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Terminación</p>
											</div>
											<div className="column is-three-quarters is-field">
												<p className="is-size-7">{this.state.terminacion}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-fifth">
										<div className="columns is-multiline">
											<div className="column is-full is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">Cant.</p>
											</div>
											<div className="column is-full is-field">
												<p className="is-size-7 has-text-centered">{`${String(this.state.cantidad).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}${this.state.cantidad_count}`}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-fifth">
										<div className="columns is-multiline">
											<div className="column is-full is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">VUP</p>
											</div>
											<div className="column is-full is-field">
												<p className="is-size-7 has-text-centered">{this.state.vup} {String(this.state.vup_value).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-fifth">
										<div className="columns is-multiline">
											<div className="column is-full is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold has-text-danger">Dep. Ross-Heidecke</p>
											</div>
											<div className="column is-full is-field">
												<p className="is-size-7 has-text-centered has-text-danger">{this.state.porcentaje_drh}%</p>
											</div>
										</div>
									</div>
									<div className="column is-one-fifth">
										<div className="columns is-multiline">
											<div className="column is-full is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">V. U. Dep</p>
											</div>
											<div className="column is-full is-field">
												<p className="is-size-7 has-text-centered">{String(this.vu_dep()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
									<div className="column is-one-fifth">
										<div className="columns is-multiline">
											<div className="column is-full is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">S-Total</p>
											</div>
											<div className="column is-full is-field">
												<p className="is-size-7 has-text-centered">{String(this.stotal()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Otras mejoras</h2>
									</div>
									
									{this.state.otras_mejoras_data[0] ? this.state.otras_mejoras_data.map((a, i) => {
										return(
											<>
												<div className="column is-12">
													<div className="columns is-multiline">
														<div className="column is-one-fifth is-label">
															<p className="has-text-right is-size-7 has-text-weight-bold">Descripción</p>
														</div>
														<div className="column is-four-fifths is-field">
															<p className="is-size-7">{a.data.descripcion}</p>
														</div>
													</div>
												</div>
												<div className="column is-one-fifth">
													<div className="columns is-multiline">
														<div className="column is-full is-label">
															<p className="has-text-centered is-size-7 has-text-weight-bold">Cant.</p>
														</div>
														<div className="column is-full is-field">
															<p className="is-size-7 has-text-centered">{a.data.cantidad}</p>
														</div>
													</div>
												</div>
												<div className="column is-one-fifth">
													<div className="columns is-multiline">
														<div className="column is-full is-label">
															<p className="has-text-centered is-size-7 has-text-weight-bold">VUP</p>
														</div>
														<div className="column is-full is-field">
															<p className="is-size-7 has-text-centered">{a.data.vup} {a.data.vup_value}</p>
														</div>
													</div>
												</div>
												<div className="column is-one-fifth">
													<div className="columns is-multiline">
														<div className="column is-full is-label">
															<p className="has-text-centered is-size-7 has-text-weight-bold has-text-danger">Dep. física</p>
														</div>
														<div className="column is-full is-field">
															<p className="is-size-7 has-text-centered has-text-danger">{a.data.porcentaje_dep_fisica}%</p>
														</div>
													</div>
												</div>
												<div className="column is-one-fifth">
													<div className="columns is-multiline">
														<div className="column is-full is-label">
															<p className="has-text-centered is-size-7 has-text-weight-bold">V. U. Dep</p>
														</div>
														<div className="column is-full is-field">
															<p className="is-size-7 has-text-centered">{a.data.vu_dep}</p>
														</div>
													</div>
												</div>
												<div className="column is-one-fifth">
													<div className="columns is-multiline">
														<div className="column is-full is-label">
															<p className="has-text-centered is-size-7 has-text-weight-bold">S-Total</p>
														</div>
														<div className="column is-full is-field">
															<p className="is-size-7 has-text-centered">{String(this.otras_mejoras_data_stotal(a.data.cantidad, a.data.vup_value, a.data.porcentaje_dep_fisica)).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
														</div>
													</div>
												</div>
											</>
										)
									}):[]}
								</div>
								<div className="columns is-multiline">
									<div className="column is-full">
										<h2 className="has-text-centered is-size-5 has-text-weight-bold">Resumen de valores</h2>
									</div>
									<div className="column is-2">
										<div className="columns is-multiline">
											<div className="column is-12">
												<p className="is-size-7 has-text-centered">&nbsp;</p>
											</div>
											<div className="column is-12 is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">Terrenos</p>
											</div>
											<div className="column is-12 is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">Edificaciones</p>
											</div>
											<div className="column is-12 is-label">
												<p className="has-text-centered is-size-7 has-text-weight-bold">Otras mejoras</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-12 is-label">
												<p className="is-size-7 has-text-centered has-text-weight-bold">Valor Total</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7">RD${String(this.valor_total_ajustado_terrenos()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7">RD${String(this.stotal()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7">RD${String(this.valor_total()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-12 is-label">
												<p className="is-size-7 has-text-centered has-text-weight-bold has-text-danger">Otros ajustes</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7 has-text-danger">{this.state.otros_ajustes_a}</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7 has-text-danger">{this.state.otros_ajustes_b}</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7 has-text-danger">{this.state.otros_ajustes_x}</p>
											</div>
										</div>
									</div>
									<div className="column is-1">
										<div className="columns is-multiline">
											<div className="column is-12 is-label">
												<p className="is-size-7 has-text-centered has-text-weight-bold has-text-danger">%</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7 has-text-danger">{this.state.porcentaje_a}%</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7 has-text-danger">{this.state.porcentaje_b}%</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7 has-text-danger">{this.state.porcentaje_x}%</p>
											</div>
										</div>
									</div>
									<div className="column">
										<div className="columns is-multiline">
											<div className="column is-12 is-label">
												<p className="is-size-7 has-text-centered has-text-weight-bold">Valor total ajustado</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7">RD${String(this.valor_total_ajustado_a()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7">RD${String(this.valor_total_ajustado_b()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
											<div className="column is-12 is-field">
												<p className="has-text-centered is-size-7">RD${String(this.valor_total_ajustado_x()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
										</div>
									</div>
									<div className="column is-12">
										<div className="columns is-multiline">
											<div className="column is-one-quarter is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Valor total inmueble</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7">RD${String(this.valor_total_inmueble()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</p>
											</div>
											<div className="column is-one-quarter is-label">
												<p className="has-text-right is-size-7 has-text-weight-bold">Valor total ajustado</p>
											</div>
											<div className="column is-one-quarter is-field">
												<p className="is-size-7 has-text-weight-bold">RD${this.state.valor_total_ajustado_input}</p>
											</div>
											<div className="column is-full">
												<h2 className="has-text-centered is-size-6 has-text-weight-bold">{this.state.valor_en_texto}</h2>
											</div>
										</div>
									</div>
								</div>
							</>
						)}
						{/*
              <!-- page 2 end -->
              <!-- page 3 start -->
            */}
						
						
						
						<div className="divider"></div>
						{/*<!-- AQUI VA EL CONTENIDO DE LA PAGINA 3 -->*/}
						
						{this.state.page_three && this.state.page_three.active ? (
							<>
								<div className="columns is-multiline">
									<div className="column is-one-quarter">
										<figure className="image is-4by3">
											<img src={`file://${this.state.page_three.save_path}`}/>
										</figure>
									</div>
								</div>
							</>
						) : []}
						
						{/*
              <!-- page 3 end -->
              <!-- page 4 start -->
            */}
						<div className="divider"/>
						<div className="columns is-multiline">
							<div className="column is-full">
								<h2 className="has-text-centered is-size-5 has-text-weight-bold">Imágenes</h2>
							</div>
							{this.state.imagenes.data ? this.state.imagenes.data.map((d, i)=>{
								return (
									<div className="column is-one-quarter">
										<figure className="image is-4by3">
											<img src={`file://${d.data.save_path}`}/>
										</figure>
									</div>
								)
							}) : []}
						</div>
						{/*
              <!-- page 4 end -->
              <!-- page 5 start -->
            */}
						<div className="divider"/>
						
						<div className="columns is-multiline">
							<div className="column is-full">
								<h2 className="has-text-centered is-size-5 has-text-weight-bold">Documentos</h2>
							</div>
						</div>
						{this.state.documentos.data ? this.state.documentos.data.map((d, i)=>{
							let p = i + 1
							
							return (
								<>
									<div key={i} className="columns is-multiline">
										<div className="column is-full">
											<figure className="image is-4by5">
												<img src={'file://' + d.data.save_path}/>
											</figure>
										</div>
									</div>
									{this.state.documentos.data.length === p ? [] : (<div className="divider"/>)}
								</>
							)
						}): []}
						{/*
              <!-- page 5 end -->
              <!-- page 6 start -->
            */}
					
					</div> {/*<!-- Result -->*/}
				
				</div> {/*<!-- Container -->*/}
			</div>
		)
	}
}
