import React from 'react'
import { getStore } from '../../../lib/reduxito'
import { getPreparedProperties, Base } from '../../../lib/utils'

export default class Status extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      status: '',
      porcentaje: 0,
      data: {
        step1: undefined,
        step2: undefined,
        step3: undefined,
        step4: undefined,
        step5: undefined,
        step6: undefined,
        step7: undefined,
        step8: undefined
      }
    }
    this.handleChange = this.handleChange.bind(this)
    this.save = this.save.bind(this)
    this.calculatePercentage = this.calculatePercentage.bind(this)
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    let data = this.state.data
    data[`step${this.props.page}`] = value
    await this.setState({data})
    this.calculatePercentage()
  }

  async calculatePercentage() {
    //64*10*0.2-28
    let n = 0
    let keys = Object.keys(this.state.data)
    for (let key of keys){
      if (Number(this.state.data[key])){
        n = n + Number(this.state.data[key])
      }
    }
    await this.setState({
      porcentaje: Number(n + 2) * 10,
      status: Number(n + 2) * 10 === 100 ? 'complete': 'incomplete'
    })
    this.save()
  }

  async save(){
    await getStore((store) => {
      let proyecto = new Base(store.db.proyecto)
      if (this.props.id){
        proyecto.update(this.state, store.db, {id: this.props.id})
      }
    })
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('Status EDITING ----->', data)

      await this.setState(getPreparedProperties(['proyecto'], data))
      console.log('STATE FORM STATUS', this.state, this.props)
    }
  }

  render(){
    return (
      <div className="level-item">
        <div className="field is-narrow">
          <div className="control">
          {this.props.page && this.props.id ? (
            <div className="select is-fullwidth">
              <select name="step" onChange={this.handleChange} value={this.state.data[`step${this.props.page}`]}>
                <option value="">Seleccionar estado</option>
                <option value="1">Completo</option>
                <option value="0">Incompleto</option>
              </select>
            </div>
          ): []}
          </div>
        </div>
      </div>
    )
  }
}
