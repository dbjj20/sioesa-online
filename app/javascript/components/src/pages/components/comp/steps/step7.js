import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'
import keygen from 'keygen'
import ImgEdit from './blank'
const main_path = '/Users/dylanjonatanbaezjimenez/privdir/Backup_sioesa_test/sioesa/imagenes/'
// const fs = window.require('fs')

export default class Step7 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      data: [],
      backup_path: '',
      project_name: '',
      isActiveDropDown: '',
      modal: false,
      imageModal: false,
      current_path: '',
      current_filename: '',
      principal: '',
    }
    this.handleChangeImg = this.handleChangeImg.bind(this)
    this.addDataToElement = this.addDataToElement.bind(this)
    this.removeElement = this.removeElement.bind(this)
    this.handleChangeImg = this.handleChangeImg.bind(this)
  }

  async addDataToElement(obj, arr, name){
    let state = obj
    let provitional = []
    let current = {}
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id === obj.id){
        Object.assign(current, a)
      }
      console.log(a)
    })
    current = state

    provitional.map((a, i) => {
      if (a.id !== obj.id){
        result.push(a)
      }
      console.log(a)
    })

    result.push(current)
    console.log(result)
    await this.setState({[name]: result})

    console.log(obj, provitional)
    console.log(current)

  }

  async removeElement(id, arr, name){
    // await this.setState({[id]: "no"})
    let provitional = []
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id !== id){
        result.push(a)
      }
      console.log(a)
    })
    console.log(result)
    await this.setState({[name]: result})
  }

  async addElement(arr, name, data){
    let provitional = []
    Object.assign(provitional, arr)

    const id = keygen.hex(keygen.small)
    await this.setState({[id]: ''})
    provitional.push({key: "", value: "", id: id, data: data || {}, active: true })
    console.log(provitional)
    await this.setState({[name]: provitional})
  }

  async handleChangeImg(e){
    if (this.state.backup_path && this.state.project_name){
      let local_path = this.state.backup_path
      let im = []
        if (true){ //!fs.existsSync(`${local_path}/${this.state.project_name}/imagenes/`)
          console.log("CREATING PATH")
          // fs.mkdirSync(`${local_path}/${this.state.project_name}/imagenes/`, { recursive: true })
        }

        for (let f of e.target.files) {
          console.log('File(s) you dragged here: ', f.path)
          console.log(f)
          let extension = f.type.split('/')
          if(extension[0] === "image"){
            let name = `img-${keygen.hex()}.${extension[1]}`
            let img = `${local_path}/${this.state.project_name}/imagenes/${name}`
            // fs.copyFile(f.path, img, (err) => {
            //   if (err) throw err;
            // })
            let obj = {name: f.name, original_path: f.path, type: f.type, size: f.size, save_path: img, formated_name: name}
            if (this.state.data[0]){
              this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: obj || {} }, this.state.data, 'data')
            }else {
              this.addElement(this.state.data, 'data', obj)
            }
          }
        }
    } else {
        alert("No hay un ruta definida de backup y/o nombre de proyecto")
    }
  }

  async componentDidMount(){
    // validacion, descripcion_legal, descripcion_inmueble, colindancias
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 7 EDITING ----->', data)

      await this.setState(getPreparedProperties(['images', 'init'], data))
      let p = getPreparedProperties(['proyecto'], data)
      p.p_data = p.data
      delete p.data
      await this.setState(p)
      console.log(this.state)
    }
    //inside of includeUI v
    // locale: locale_ru_RU,
    // theme: blackTheme, // or whiteTheme
  }

  render(){
    return(
      <div >
        <div className="container min-height" onResize={() => console.log("CAMBIANDO DE TAMANO")}>
          <h2 className="title is-4">
            Agregar imagenes
          </h2>
          <div className="divider"/>
          <div className="field">
            <div className="file is-medium is-boxed is-centered">
              <input className="button" id="drag" name='foto' type="file" accept="image/*" multiple={true} onDrop={this.handleChangeImg} onChange={this.handleChangeImg} placeholder="Subir imagenes o soltar aquí" />
            </div>
          </div>
          <div className="divider"/>
          <div className="columns is-multiline is-variable is-1">
            {this.state.data[0] ? this.state.data.map((d, i) => {
              return (
                <div key={i} className={`column is-one-fifth dropdown ${this.state.isActiveDropDown === d.id ? 'is-active' : ''}`} onMouseOver={async ()=> await this.setState({isActiveDropDown: d.id})} onMouseOut={async ()=> await this.setState({isActiveDropDown: ''})}>
                  <div className="dropdown-trigger">
                    <figure className="image is-4by3"> {/*css class mage-principal */}
                      <img src={ 'file://'+ d.data.save_path} onClick={async () => {await this.setState({imageModal: true})}}/>
                    </figure>
                    <span className="is-size-7"/>
                  </div>
                  <div className="dropdown-menu" id="image-1" role="menu">
                    <div className="dropdown-content">
                      <a className="dropdown-item" onClick={async () => {await this.setState({principal: d.id})}}>
                        Hacer imagen principal
                      </a>
                      <a className="dropdown-item" onClick={async() => {await this.setState({modal: true, current_path: d.data.save_path, current_filename: d.data.formated_name})}}>
                        Editar imagen
                      </a>
                      <hr className="dropdown-divider"/>
                      <a href="#" className="dropdown-item has-text-danger" onClick={()=> this.removeElement(d.id, this.state.data, 'data')}>
                        Borrar imagen
                      </a>
                    </div>
                  </div>
                  {this.state.modal && this.state.current_path ? (
                    <div className="modal is-active">
                      <div className="modal-background"/>
                      <div className="modal-card" style={{width: "60%"}}>
                        <header className="modal-card-head">
                          <button className="delete is-medium" aria-label="close" onClick={async() => {await this.setState({modal: false, current_path: '', current_filename:''})}}/>
                        </header>
                        <section className="modal-card-body">
                          <ImgEdit path={this.state.current_path} filename={this.state.current_filename}/>
                        </section>
                      </div>
                    </div>
                    ):[]}
                    {this.state.imageModal ? (
                      <div className="modal is-active">
                        <div className="modal-background"/>
                        <div className="modal-card" style={{width: "80%", height: "100%"}}>
                          <header className="modal-card-head">
                            <button className="delete is-medium" aria-label="close" onClick={async() => {await this.setState({imageModal: false})}}/>
                          </header>
                          <section className="modal-card-body">
                            <img src={'file://'+ d.data.save_path}/>
                          </section>
                        </div>
                      </div>
                      ):[]}
                    {this.state.principal === d.id ? 'Es imagen principal': ''}
                </div>
              )
            }): (<h1>No hay imagenes</h1>)}
          </div>
        </div>
      </div>
    )
  }
}
