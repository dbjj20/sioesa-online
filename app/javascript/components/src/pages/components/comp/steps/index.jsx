import React from 'react'
import Step1 from './step1'
import Step2 from './step2'
import Step3 from './step3'
import Step4 from './step4'
import Step5 from './step5'
import Step6 from './step6'
import Step7 from './step7'
import Step8 from './step8'
import Step9 from './step9'
import Step10 from './step10'
import Status from './status'
import { getStore } from '../../../lib/reduxito'
import { prepareProperties } from '../../../lib/utils'
import moment from 'moment'
const date_format = "dddd D [de] MMMM yyyy"

export default class Steps extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      stepPage: 1,
      proyecto_id: undefined,
      new: false,
      db_data: {},
      step1Keys: ["proyecto","validacion", "descripcion_legal", "descripcion_inmueble", "colindancias"]
    }

    this.changeStep = this.changeStep.bind(this)
    this.print = this.print.bind(this)
    this.save = this.save.bind(this)
    this.savetoRails = this.savetoRails.bind(this)
    this.loaDataFor = this.loaDataFor.bind(this)

    this.Step1 = React.createRef()
    this.Step2 = React.createRef()
    this.Step3 = React.createRef()
    this.Step4 = React.createRef()
    this.Step5 = React.createRef()
    this.Step6 = React.createRef()
    this.Step7 = React.createRef()
    this.Step8 = React.createRef()
    this.Step9 = React.createRef()
    this.Step10 = React.createRef()
  }

  async savetoRails(store, proyecto_id, values){
    let pro_data = await store.dataFetch({}, `/project/${proyecto_id}`)
    let pro_ = {}
    Object.assign(pro_, values.proyecto)
    delete values.proyecto

    let update_data = {}
    if(pro_data.data.data){
      Object.assign(pro_data.data.data, values)
      Object.assign(update_data, pro_data.data.data)
    }else {
      update_data = Object.assign({}, values)
    }

    let tosave = {home: { data: update_data }}
    delete pro_.data
    Object.assign(tosave.home, pro_)
    await store.dataFetch(tosave, `/update/project/${this.state.proyecto_id}`)
  }

  async loaDataFor(){
    let db_data
    await getStore(async (store) => {
      db_data = await store.getAllModelsDataByProjectId(this.props.proyecto_id, store)
    })
    return db_data
  }

  print(){
    window.print()
  }

  async save(){
    let refState
    let keys
    let proPage = this.state.stepPage
    switch (this.state.stepPage) {
      case 1:
        refState = this.Step1.current
        keys = this.state.step1Keys
        await getStore(async (d) => {
          //refState.state is the object that contains the data
          refState.state.proyecto_id = this.state.proyecto_id
          let values = prepareProperties(refState.state, d.db_state, keys)
          // TODO: agregar logica de proyect id y mandarle eso a prepareProperties para que se lo ponga a todos los models
          // luego hacer if this.state.proyecto_id update else add


          values.proyecto.name = values.descripcion_legal.nombre_propietario || "Tasación editada"
          values.proyecto.direccion = values.descripcion_inmueble.direccion_del_inmueble || 'Dirección del cliente'
          values.proyecto.by_pname = values.proyecto.project_name || ''
          values.proyecto.by_prop_name = values.descripcion_legal.nombre_propietario || ''
          values.proyecto.by_cedula = values.descripcion_legal.cedula || ''
          values.proyecto.by_mu_name = values.descripcion_legal.municipio_name || ''
          values.proyecto.by_rnc = values.descripcion_legal.requerido_por_rnc || ''
          values.proyecto.by_prov_name = values.descripcion_legal.provincia_name || ''

          delete values.proyecto.id

          await this.savetoRails(d, this.state.proyecto_id, values)
        })
      break;

      case 2:
        refState = this.Step2.current
        keys = ['sector_inmueble']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 2 ADD ---->', values.sector_inmueble)
          await this.savetoRails(store, this.state.proyecto_id, values)
        })
      break;

      case 3:
        refState = this.Step3.current
        keys = ['servicios_disponibles','vias_acceso']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 3 ADD ---->', values)
          await this.savetoRails(store, this.state.proyecto_id, values)
          await this.setState({new: false})
        })
      break;

      case 4:
        refState = this.Step4.current
        keys = ['caracteristicas']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 4 ADD ---->', values.caracteristicas)
          await this.savetoRails(store, this.state.proyecto_id, values)
        })
        await this.setState({stepPage: proPage})
      break;

      case 5:
        refState = this.Step5.current
        // await this.setState({stepPage: 0})
        keys = ['ofertas_ventas_comparables']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 5 ADD ---->', values.ofertas_ventas_comparables)
          await this.savetoRails(store, this.state.proyecto_id, values)
          await this.setState({new: false})
        })
        // await this.setState({stepPage: proPage})
      break;

      case 6:
        refState = this.Step6.current
        keys = ['caracteristicas_edificacion_mejora']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 6 ADD ---->', values.caracteristicas_edificacion_mejora)
          await this.savetoRails(store, this.state.proyecto_id, values)
          await this.setState({new: false})
        })
      break;

      case 7:
        refState = this.Step7.current
        keys = ['images']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 7 ADD ---->', values.images)
          await this.savetoRails(store, this.state.proyecto_id, values)
          await this.setState({new: false})
        })
      break;

      case 8:
        refState = this.Step8.current
        // await this.setState({stepPage: 0})
        keys = ['documentos']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 7 ADD ---->', values.documentos)
          await this.savetoRails(store, this.state.proyecto_id, values)
          await this.setState({new: false})
        })
        // await this.setState({stepPage: proPage})
      break;

      case 9:
        refState = this.Step9.current
        // await this.setState({stepPage: 0})
        keys = ['pages']
        refState.state.proyecto_id = this.state.proyecto_id

        await getStore(async (store) => {
          let values = prepareProperties(refState.state, store.db_state, keys)
          console.log('STEP 9 ADD ---->', values)
          await this.savetoRails(store, this.state.proyecto_id, values)
          await this.setState({new: false})
        })
        // await this.setState({stepPage: proPage})
      break;

      case 10:
        refState = this.Step10.current
      break;

      default:
      refState = {noRefState: true}
    }
  }

  async changeStep(action){
    if (action && action === 'next'){
      if (this.state.stepPage === 10) return
      await this.setState({
        stepPage: this.state.stepPage + 1
      })
    }

    if (action && action === 'back'){
      if (this.state.stepPage === 1) return
      await this.setState({
        stepPage: this.state.stepPage - 1
      })
    }
  }

  async componentDidMount(){
    let date = moment().format()
    if (this.props.print){
      await this.setState({stepPage: 10})
    }
    try {
      await getStore( async (store) => {
        let pro
        if (this.props.proyecto_id){
          // pro = await store.db.proyecto.where("id").equals(this.props.proyecto_id).toArray()
          pro = await store.dataFetch({}, `/project/${this.props.proyecto_id}`)
        }else {
          // pro = await store.db.proyecto.where("name").equals("none").toArray()
          pro = await store.dataFetch({}, `/search/name/none`)
        }
        // let pro = await store.db.proyecto.where("name").equals("none").toArray()
        console.log("PROYECTO PRO PRO =------>", pro)
        let data = pro.data

        if (data.id){
          await this.setState({
            proyecto_id: data.id, new: !this.props.proyecto_id,
           })
        }else {

          console.log("CREATING PROYECT")
          let result = await store.dataFetch({
            home: {
              name: 'none',
              fecha: moment().format(date_format),
              direccion: 'N/A',
              project_name: '',
              status: '',
              porcentage: 0,
              progres: {
                step1: '',
                step2: '',
                step3: '',
                step4: '',
                step5: '',
                step6: '',
                step7: '',
                step8: ''
              },
            }
          }, '/add/project')
          await this.setState({proyecto_id: result.id})
          // console.log(result, this.state)
        }
      })
    } catch (e) {
      console.log(e, e.message)
    }

    // console.log(this.state)
  }

  render(){
    return(
      <div>
          <nav className="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li><a onClick={this.props.cancel}>Tasaciones</a></li>
              <li className="is-active"><a href="#" aria-current="page">{this.props.proyecto_id}</a></li>
            </ul>
          </nav>

          <div className="level">
            <div className="level-left">
              <div className="level-item">
                <h1 className="title is-1">
                  Paso {this.state.stepPage} de 10
                </h1>
              </div>
            </div>
            {this.state.stepPage !== 10 ? (
              <div key="1" className="level-right">
                {this.state.stepPage === 1 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 2 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 3 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 4 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 5 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 6 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 7 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 8 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                <p className="level-item">
                  <button onClick={this.save} className="button is-info is-outlined">
                    <span className="icon is-small">
                      <i className="fas fa-save"/>
                    </span>
                    <span>Guardar</span>
                  </button>
                </p>
                <p className="level-item">
                  <button className="button is-danger is-outlined" onClick={this.props.cancel}>
                    <span>Cancelar</span>
                    <span className="icon is-small">
                      <i className="fas fa-times"/>
                    </span>
                  </button>
                </p>
              </div>
            ):(
                <div key="2" className="level-right">
                  <p className="level-item">
                    <button className="button is-success is-outlined" onClick={this.print}>
                      <span className="icon is-small">
                        <i className="fas fa-print"/>
                      </span>
                      <span>Imprimir o Guardar PDF</span>
                    </button>
                  </p>
                </div>
            )}
          </div>

          <progress className="progress is-info" value={this.state.stepPage * 10} max="100">{this.state.stepPage * 10}%</progress>
          {/*<MuiPickersUtilsProvider utils={MomentUtils}>*/}{/*date picket context*/}
            {this.state.stepPage === 1 ? (<Step1 date_format={date_format} ref={this.Step1} step1Keys={this.state.step1Keys} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>) : []}
            {this.state.stepPage === 2 ? (<Step2 date_format={date_format} ref={this.Step2} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 3 ? (<Step3 date_format={date_format} ref={this.Step3} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 4 ? (<Step4 date_format={date_format} ref={this.Step4} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 5 ? (<Step5 date_format={date_format} ref={this.Step5} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 6 ? (<Step6 date_format={date_format} ref={this.Step6} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 7 ? (<Step7 date_format={date_format} ref={this.Step7} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 8 ? (<Step8 date_format={date_format} ref={this.Step8} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 9 ? (<Step9 date_format={date_format} ref={this.Step9} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 10 ? (<Step10 date_format={date_format} ref={this.Step10} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>):[]}
          {/*</MuiPickersUtilsProvider>*/}

          <nav className="pagination is-centered is-fullwidth" role="navigation" aria-label="pagination">
            <a className="pagination-previous"  onClick={() => this.changeStep('back')}><span className="icon is-small">
                <i className="fas fa-chevron-left"/>
              </span>Anterior
            </a>
            <a className="pagination-next"  onClick={() => this.changeStep('next')}>Próximo <span className="icon is-small">
                <i className="fas fa-chevron-right"/>
              </span>
            </a>
            <ul className="pagination-list">
              <li><a  className={`pagination-link ${this.state.stepPage === 1 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 1})} aria-label="Goto page 1">1</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 2 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 2})} aria-label="Goto page 2">2</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 3 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 3})} aria-label="Goto page 3">3</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 4 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 4})} aria-label="Goto page 4">4</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 5 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 5})} aria-label="Goto page 5">5</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 6 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 6})} aria-label="Goto page 6">6</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 7 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 7})} aria-label="Goto page 7">7</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 8 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 8})} aria-label="Goto page 8">8</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 9 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 9})} aria-label="Goto page 9">9</a></li>
              <li><a  className={`pagination-link ${this.state.stepPage === 10 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 10})} aria-label="Goto page 10">10</a></li>
            </ul>
          </nav>
      </div>
    )
  }
}
