import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'
import keygen from 'keygen'
// import convertPdfToPng from '../../../lib/pdf'
// const fs = window.require('fs')

export default class Step9 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      backup_path: '',
      project_name: '',
      page_one: {
        name: "",
        original_path: "",
        type: "",
        size: 0,
        save_path: "",
        formated_name: "",
        active: false
      },
      page_two: {
        name: "",
        original_path: "",
        type: "",
        size: 0,
        save_path: "",
        formated_name: "",
        active: false
      },
      page_three: {
        name: "",
        original_path: "",
        type: "",
        size: 0,
        save_path: "",
        formated_name: "",
        active: false
      }
    }
    this.handleChangePage = this.handleChangePage.bind(this)
    this.cleanPage = this.cleanPage.bind(this)
    this.handleChangeDocument = this.handleChangeDocument.bind(this)
  }
  async cleanPage(name){
    await this.setState({
      [name]: {
        name: "",
        original_path: "",
        type: "",
        size: 0,
        save_path: "",
        formated_name: "",
        active: false
      }
    })
  }

  async handleChangePage(name, key, value){
    let provitional = this.state[name]
    provitional[key] = value
    await this.setState({[name]: provitional})
  }

  async handleChangeDocument(e){
    const page_name = e.target.name;
    if (this.state.backup_path && this.state.project_name){
      let local_path = this.state.backup_path
      let im = []
        if (true){//!fs.existsSync(`${local_path}/${this.state.project_name}/paginas/`)
          console.log("CREATING PATH")
          // fs.mkdirSync(`${local_path}/${this.state.project_name}/paginas/`, { recursive: true })
        }

        for (let f of e.target.files) {
          console.log('File(s) you dragged here: ', f.path)
          console.log(f)
          let extension = f.type.split('/')
          console.log(extension)
          if (extension[0] === 'image'){

            let name = `img-${keygen.hex()}.${extension[1]}`
            let img = `${local_path}/${this.state.project_name}/paginas/${name}`
            // fs.copyFile(f.path, img, (err) => {
            //   if (err) throw err;
            // })
            let obj = {name: f.name, original_path: f.path, type: f.type, size: f.size, save_path: img, formated_name: name, active:false}
            await this.setState({[page_name]: obj})
            // if (this.state.data[0]){
            //   this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: obj || {} }, this.state.data, 'data')
            // }else {
            //   this.addElement(this.state.data, 'data', obj)
            // }
          }else if (extension[0].match('application') && extension[1].match('pdf')) {

            let name = `pdf-${keygen.hex()}.${extension[1]}`
            let pdf = `${local_path}/${this.state.project_name}/paginas/${name}`
            // fs.copyFile(f.path, pdf, (err) => {
            //   if (err) throw err;
            // })
            let obj = {name: f.name, original_path: f.path, type: f.type, size: f.size, save_path: pdf, formated_name: name}
            // if (this.state.data[0]){
              const pdf_file = f
              const callback = images => {
                  // the function returns an array
                  // every img is a normal file object
                  images.forEach(async img => {
                      console.log(img)
                      const buffer = Buffer.from(await new Response(img).arrayBuffer())
                      // fs.writeFile(`${local_path}/${this.state.project_name}/paginas/${pdf_file.name}--${img.name}`, buffer, function (err) {
                      //   if (err) throw err;
                      //   console.log('Saved!');
                      // })
                      // this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: {
                      //   name: `${pdf_file.name}--${img.name}`,
                      //   type: 'image',
                      //   save_path: `${local_path}/${this.state.project_name}/paginas/${pdf_file.name}--${img.name}`
                      // } }, this.state.data, 'data')
                      await this.setState({[page_name]: {key: "", value: "", id: keygen.hex(keygen.small), name: `${pdf_file.name}--${img.name}`,
                        type: 'image',
                        save_path: `${local_path}/${this.state.project_name}/paginas/${pdf_file.name}--${img.name}`,
                        active: false
                      }})
                  })
              }
              // convertPdfToPng(pdf_file, {
              //     outputType: 'callback',
              //     callback: callback
              // })
              // this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: obj || {} }, this.state.data, 'data')
            // }else {
            //   const pdf_file = f
            //   const callback = images => {
            //       // the function returns an array
            //       // every img is a normal file object
            //       images.forEach(async img => {
            //           console.log(img)
            //           const buffer = Buffer.from(await new Response(img).arrayBuffer())
            //           fs.writeFile(`${local_path}/${this.state.project_name}/paginas/${pdf_file.name}--${img.name}`, buffer, function (err) {
            //             if (err) throw err;
            //             console.log('Saved!');
            //           })
            //           // this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: {
            //           //   name: `${pdf_file.name}--${img.name}`,
            //           //   type: 'image',
            //           //   save_path: `${local_path}/${this.state.project_name}/paginas/${pdf_file.name}--${img.name}`
            //           // } }, this.state.data, 'data')
            //           await this.setState({[page_name]: {key: "", value: "", id: keygen.hex(keygen.small), name: `${pdf_file.name}--${img.name}`,
            //             type: 'image',
            //             save_path: `${local_path}/${this.state.project_name}/paginas/${pdf_file.name}--${img.name}`,
            //             active: false
            //           }})
            //       })
            //   }
            //   convertPdfToPng(pdf_file, {
            //       outputType: 'callback',
            //       callback: callback
            //   })
            // }

          }else{
            alert("Solo imagenes o archivos PDF")
          }
        }
    } else {
        alert("No hay un ruta definida de backup y/o nombre de proyecto")
    }
    console.log(this.state)
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 9 EDITING ----->', data)

      await this.setState(getPreparedProperties(['pages', 'init', 'proyecto'], data))
      console.log(this.state)
    }
  }

  render(){
    return(
      <div >
        <form className="container min-height">
          <h2 className="title is-4">
            Agregar / Remplazar páginas
          </h2>
          <div className="divider"/>
          <div className="columns">
            <div className="column">
              <h3 className="title is-5 has-text-weight-light has-text-centered">
                Página 1
              </h3>
              <div className="file has-name is-centered is-boxed">
                <label className="file-label">
                  <input className="button" id="drag" name='page_one' type="file" accept="*" multiple={true} onDrop={this.handleChangeDocument} onChange={this.handleChangeDocument} placeholder="Subir o soltar documentos aquí" />
                  <span className="file-cta">
                    {this.state.page_one.save_path ? (<img src={"file://" + this.state.page_one.save_path} />): []}
                  </span>
                  <span className="file-name">
                    {this.state.page_one.name}
                  </span>
                  Activada? - {this.state.page_one.active ? 'Si': 'No'}
                  <input type="checkbox" checked={this.state.page_one.active} onChange={() => this.handleChangePage('page_one', 'active', !this.state.page_one.active)}/>
                </label>
              </div>
            </div>
            <div className="column">
              <h3 className="title is-5 has-text-weight-light has-text-centered">
                Página 2
              </h3>
              <div className="file has-name is-centered is-boxed">
                <label className="file-label">
                  <input className="button" id="drag" name='page_two' type="file" accept="*" multiple={true} onDrop={this.handleChangeDocument} onChange={this.handleChangeDocument} placeholder="Subir o soltar documentos aquí" />
                  <span className="file-cta">
                    {this.state.page_two.save_path ? (<img src={"file://" + this.state.page_two.save_path} />): []}
                  </span>
                  <span className="file-name">
                    {this.state.page_two.name}
                  </span>
                  Activada? - {this.state.page_two.active ? 'Si': 'No'}
                  <input type="checkbox" checked={this.state.page_two.active} onChange={() => this.handleChangePage('page_two', 'active', !this.state.page_two.active)}/>
                </label>
              </div>
            </div>
            <div className="column">
              <h3 className="title is-5 has-text-weight-light has-text-centered">
                Página 3
              </h3>
              <div className="file has-name is-centered is-boxed">
                <label className="file-label">
                  <input className="button" id="drag" name='page_three' type="file" accept="*" multiple={true} onDrop={this.handleChangeDocument} onChange={this.handleChangeDocument} placeholder="Subir o soltar documentos aquí" />
                  <span className="file-cta">
                    {this.state.page_three.save_path ? (<img src={"file://" + this.state.page_three.save_path} />): []}
                  </span>
                  <span className="file-name">
                    {this.state.page_three.name}
                  </span>
                  Activada? - {this.state.page_three.active ? 'Si': 'No'}
                  <input type="checkbox" checked={this.state.page_three.active} onChange={() => this.handleChangePage('page_three', 'active', !this.state.page_three.active)}/>
                </label>
              </div>
            </div>
          </div>
        </form>
        <div className="divider"/>
      </div>
    )
  }
}
