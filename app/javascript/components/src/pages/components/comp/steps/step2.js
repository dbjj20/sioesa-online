import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'

export default class Step2 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      proyecto_id: Number,
      locacion: 'Urbano',
      uso_de_suelo: 'Agricola',
      porcentaje_de_solares_edificados: '',
      tipo_de_edificacion: 'Neoclasica',
      clase_social: 'Baja',
      factores_favorables: '',
      factores_desfavorables: '',
      crecimiento: 'Nulo',
      conservacion: 'Excelente, A',
      cambio_de_uso: 'Poco',
      edad_aparente: '',
      demanda_oferta: 'Alta',
      deseabilidad: 'Alta',
      otros: '',
      created_at: '',
      updated_at: '',
      created_by_detail: '',
      updated_by_detail: ''
    }
    this.handleChange = this.handleChange.bind(this)
    // sector_inmueble
  }
  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 2 EDITING ----->', data)

      await this.setState(getPreparedProperties(['sector_inmueble'], data))
      console.log(this.state)
    }
  }

  render(){
    return(
      <div>
        <div className="container min-height">
            <h2 className="title is-3 has-text-weight-light">
              Vecindad y/o sector del inmueble
            </h2>
            <h3 className="subtitle is-6">(Factores predominantes)</h3>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is-offset-1">
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Locación</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select value={this.state.locacion}  name="locacion" onChange={this.handleChange}>
                            <option value="Urbano">Urbano</option>
                            <option value="Suburbano">Suburbano</option>
                            <option value="Rural">Rural</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Uso de suelo</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="uso_de_suelo"  value={this.state.uso_de_suelo} onChange={this.handleChange}>
                            <option value="Agricola">Agricola</option>
                            <option value="Comercial">Comercial</option>
                            <option value="Residencial">Residencial</option>
                            <option value="Mixto">Mixto</option>
                            <option value="Turistico">Turistico de playa</option>
                            <option value="Turistico">Turistico de montaña</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Tipo de edificaciones</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="tipo_de_edificacion"  value={this.state.tipo_de_edificacion} onChange={this.handleChange}>
                            <option value="Neoclasica">Neoclasica</option>
                            <option value="Modernas">Modernas</option>
                            <option value="Victoriana">Victoriana</option>
                            <option value="Colonial">Colonial</option>
                            <option value="Postmoderna">Postmoderna</option>
                            <option value="Contemporanea">Contemporanea</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Clase social</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="clase_social"  value={this.state.clase_social} onChange={this.handleChange}>
                            <option value="Baja">Baja</option>
                            <option value="Baja">Baja-media</option>
                            <option value="Media">Media</option>
                            <option value="Media">Media-alta</option>
                            <option value="Alta">Alta</option>
                            <option value="Mixta">Mixta</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Crecimiento</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="crecimiento"  value={this.state.crecimiento} onChange={this.handleChange}>
                            <option value="Nulo">Nulo</option>
                            <option value="Lento">Lento</option>
                            <option value="Integración">Integración</option>
                            <option value="Estable">Estable</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Conservación</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="conservacion"  value={this.state.conservacion} onChange={this.handleChange}>
                            <option value="Excelente, A">Excelente, A</option>
                            <option value="Muy bueno,">Muy bueno, B</option>
                            <option value="Bueno, C">Bueno, C</option>
                            <option value="Normal, D">Normal, D</option>
                            <option value="Regular, E">Regular, E</option>
                            <option value="Malo, F">Malo, F</option>
                            <option value="Muy malo,">Muy malo, G</option>
                            <option value="Demolición, H">Demolición, H</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Cambio de uso</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="cambio_de_uso"  value={this.state.cambio_de_uso} onChange={this.handleChange}>
                            <option value="Poco">Poco probable</option>
                            <option value="Probable">Probable</option>
                            <option value="Muy">Muy probable</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Demanda / Oferta</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="demanda_oferta"  value={this.state.demanda_oferta} onChange={this.handleChange}>
                            <option value="Alta">Alta</option>
                            <option value="Equilibrada">Equilibrada</option>
                            <option value="Baja">Baja</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Deseabilidad</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="deseabilidad"  value={this.state.deseabilidad} onChange={this.handleChange}>
                            <option value="Alta">Alta</option>
                            <option value="Media">Media</option>
                            <option value="Baja">Baja</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">% de solares edificados</label>
                  </div>
                  <div className="field-body">
                    <div className="field has-addons">
                      <div className="control">
                        <input name="porcentaje_de_solares_edificados"  value={this.state.porcentaje_de_solares_edificados} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                      </div>
                      <p className="control">
                        <a className="button is-small is-static">
                          %
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Edad aparente</label>
                  </div>
                  <div className="field-body">
                    <div className="field has-addons">
                      <div className="control">
                        <input name="edad_aparente"  value={this.state.edad_aparente} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                      </div>
                      <p className="control">
                        <a className="button is-small is-static">
                          Año(s)
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Factores desfavorables</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <textarea name="factores_desfavorables"  defaultValue={this.state.factores_desfavorables} onChange={this.handleChange} className="textarea is-small" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Factores favorables</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <textarea name="factores_favorables"  defaultValue={this.state.factores_favorables} onChange={this.handleChange} className="textarea is-small" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Otros</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <textarea name="otros"  className="textarea is-small" defaultValue={this.state.otros} onChange={this.handleChange} placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}
