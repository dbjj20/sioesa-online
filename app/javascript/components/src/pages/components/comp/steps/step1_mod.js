import React from 'react'

export default class Step1Mod extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      libro: '',
      folio: ''
    }
    this.save = this.save.bind(this)
    this.handleChange = this.handleChange.bind(this)
    
  }

  async save(){
    await this.props.add({
      id: this.props.id,
      state: this.state
    })
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    console.log("DATA FROM PROPS MODULE STEP 1 MOD =---->", this.props.data)
    await this.setState(this.props.data)
  }

  render(){
    return(
      <div className="field is-horizontal">
        <div className="field-label is-small">
          <label className="label">Libro / Folio #{this.props.number}</label>
        </div>
        <div className="field-body">
          <a className="button is-small is-danger" onClick={() => this.props.removeElement(this.props.id)}>
            <i className="fas fa-minus"/>
          </a>
          &nbsp;
          <a className="button is-small is-info" onClick={this.save}>
            <i className="fas fa-save"/>
          </a>
          &nbsp;
          <div className="field has-addons">
            <p className="control">
              <input name="libro" onChange={this.handleChange} value={this.state.libro} className="input is-small" type="text" placeholder="Libro"/>
            </p>
            <p className="control is-expanded">
              <input name="folio" onChange={this.handleChange} value={this.state.folio} className="input is-small" type="text" placeholder="Folio"/>
            </p>
          </div>
        </div>
      </div>
    )
  }
}
