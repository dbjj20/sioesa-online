import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'
import Step6Module from './step6_module'
import keygen from 'keygen'
import Step6Mod from './step6_mod'

export default class Step6 extends React.Component{
  constructor(props){
    super(props)
    //caracteristicas_edificacion_mejora
    this.state = {
      proyecto_id: Number,
      cantidad_niveles_pisos: '1',
      tipo_arquitectura: 'Neoclasica',
      estado_conservacion: 'Excelente, A',
      calidad_materiales: 'Buenos',
      deseabilidad_inmueble: 'Alta',
      acorde_con_entorno: 'Si',
      habilitada: 'Si',
      edad_aparente: '0',
      vida_util_estimada: '0',
      coeficiente: '0',
      porciento_de_vida: '0',
      edad_util_remanente: '0',
      distribucion_x_uno: '',
      distribucion_x_dos: '',
      distribucion_y_uno: '',
      distribucion_y_dos: '',
      construccion: '',
      terminacion: '',
      cantidad: '',
      cantidad_count: 'm²',

      vup: 'RD$',
      vup_value: '',
      porcentaje_drh: '',
      vu_dep: '',

      vup_two: '',
      vup_value_two:'',

      stotal: '',
      descripcion: '',
      porcentaje_dep_fisica: '',
      stotal_two: '',

      valor_total_x: '',
      otros_ajustes_x: '',
      porcentaje_x: '',
      valor_total_ajustado_x: '',

      valor_total_a: '',
      otros_ajustes_a: '',
      porcentaje_a: '',
      valor_total_ajustado_a: '',

      valor_total_b: '',
      otros_ajustes_b: '',
      porcentaje_b: '',
      valor_total_ajustado_b: '',
      otras_mejoras_data: [],
      ofertas_ventas_comparables: {},
      valor_en_texto: '',
      valor_total_ajustado_input: '',
      distribucion:[]
    }

    this.handleChange = this.handleChange.bind(this)

    this.vu_dep = this.vu_dep.bind(this)
    this.stotal = this.stotal.bind(this)
    this.porciento_de_vida = this.porciento_de_vida.bind(this)
    this.valor_total_ajustado_terrenos = this.valor_total_ajustado_terrenos.bind(this)
    this.valor_total = this.valor_total.bind(this)
    this.valor_total_inmueble = this.valor_total_inmueble.bind(this)

    this.valor_total_ajustado_a = this.valor_total_ajustado_a.bind(this)
    this.valor_total_ajustado_x = this.valor_total_ajustado_x.bind(this)
    this.valor_total_ajustado_b = this.valor_total_ajustado_b.bind(this)

    this.addDataToElement = this.addDataToElement.bind(this)
    this.removeElement = this.removeElement.bind(this)
    this.addElement = this.addElement.bind(this)
  }

  valor_total_inmueble(){
    let result = parseFloat(this.valor_total_ajustado_x()) + parseFloat(this.valor_total_ajustado_b()) + parseFloat(this.valor_total_ajustado_a())
    return result.toFixed(2)
  }

  valor_total_ajustado_x(){
    let result = parseFloat(this.valor_total() - (this.valor_total() * this.state.porcentaje_x) / 100)
    return result.toFixed(2)
  }

  valor_total_ajustado_b(){
    let result = parseFloat(this.stotal() - (this.stotal() * this.state.porcentaje_b) / 100)
    return result.toFixed(2)
  }

  valor_total_ajustado_a(){
    let result = parseFloat(this.valor_total_ajustado_terrenos() - (this.valor_total_ajustado_terrenos() * this.state.porcentaje_a) / 100)
    return result.toFixed(2)
  }

  valor_total(){
    let s = 0
    let sum = this.state.otras_mejoras_data[0] ? this.state.otras_mejoras_data.map((d, i) => {
      // debugger
      s = s + parseFloat(d.data.stotal)
    }) : ''

    return s
  }

  valor_total_ajustado_terrenos(){
    let result = parseFloat(this.state.ofertas_ventas_comparables.area_total * this.state.ofertas_ventas_comparables.mediana)
    return result.toFixed(2)
  }

  porciento_de_vida(){
    let result = parseFloat((this.state.vida_util_estimada / this.state.edad_aparente) * 100)
    return result
  }

  async addDataToElement(obj, arr, name){
    let state = obj.state
    let provitional = []
    let current = {}
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id === obj.id){
        Object.assign(current, a)
      }
      console.log(a)
    })
    current.data = state

    provitional.map((a, i) => {
      if (a.id !== obj.id){
        result.push(a)
      }
      console.log(a)
    })

    result.push(current)
    console.log(result)
    await this.setState({[name]: result})

    console.log(obj, provitional)
    console.log(current)

  }

  async removeElement(id, arr, name){
    // await this.setState({[id]: "no"})
    let provitional = []
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id !== id){
        result.push(a)
      }
      console.log(a)
    })
    console.log(result)
    await this.setState({[name]: result})
  }

  async addElement(arr, name){
    let provitional = []
    Object.assign(provitional, arr)

    const id = keygen.hex(keygen.small)
    await this.setState({[id]: ''})
    provitional.push({key: "", value: "", id: id, data:{} })
    console.log(provitional)
    await this.setState({[name]: provitional})
  }

  stotal(){
    let result = parseFloat(this.vu_dep(6) * this.state.cantidad)
    return result.toFixed(2)
  }

  vu_dep(fix){
    let result = parseFloat(parseFloat(this.state.vup_value).toFixed(6) - (parseFloat(this.state.vup_value).toFixed(6) * (this.state.porcentaje_drh)) / 100)
    return result.toFixed(fix || 2)
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 6 componentDidMount ----->', data)

      await this.setState(getPreparedProperties(['caracteristicas_edificacion_mejora'], data))

      await this.setState({
        vu_dep: this.vu_dep(),
        stotal: this.stotal(),
        porciento_de_vida: this.porciento_de_vida(),
        ofertas_ventas_comparables: getPreparedProperties(['ofertas_ventas_comparables'], data)
      })
    }
  }

  render(){
    return(
      <div >
        <div className="container min-height">
          <h2 className="title is-4">
            Caracteristica de la edificación y mejoras
          </h2>
          <div className="divider"/>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Cantidad de niveles/pisos</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <span className="select is-small">
                    <select name="cantidad_niveles_pisos" value={this.state.cantidad_niveles_pisos} onChange={this.handleChange}>
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      <option>6</option>
                      <option>7</option>
                      <option>8</option>
                      <option>9</option>
                      <option>10</option>
                      <option>11</option>
                      <option>12</option>
                      <option>13</option>
                      <option>14</option>
                      <option>15</option>
                    </select>
                  </span>
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Tipo de Arquitectura</label>
            </div>
            <div className="field-body">
              <div className="field is-narrow">
                <div className="control is-expanded">
                  <div className="select is-fullwidth is-small">
                    <select name="tipo_arquitectura" value={this.state.tipo_arquitectura} onChange={this.handleChange}>
                      <option>Neoclasica</option>
                      <option>Modernas</option>
                      <option>Victoriana</option>
                      <option>Colonial</option>
                      <option>Postmoderna</option>
                      <option>Contemporanea</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Estado de conservación</label>
            </div>
            <div className="field-body">
              <div className="field is-narrow">
                <div className="control is-expanded">
                  <div className="select is-fullwidth is-small">
                    <select name="estado_conservacion" value={this.state.estado_conservacion} onChange={this.handleChange}>
                      <option>Excelente, A</option>
                      <option>Muy bueno, B</option>
                      <option>Bueno, C</option>
                      <option>Normal, D</option>
                      <option>Regular, E</option>
                      <option>Malo, F</option>
                      <option>Muy malo, G</option>
                      <option>Demolición, H</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Calidad de los materiales</label>
            </div>
            <div className="field-body">
              <div className="field is-narrow">
                <div className="control is-expanded">
                  <div className="select is-fullwidth is-small">
                    <select name="calidad_materiales" value={this.state.calidad_materiales} onChange={this.handleChange}>
                      <option>Buenos</option>
                      <option>Malos</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Deseabilidad del inmueble</label>
            </div>
            <div className="field-body">
              <div className="field is-narrow">
                <div className="control is-expanded">
                  <div className="select is-fullwidth is-small">
                    <select name="deseabilidad_inmueble" value={this.state.deseabilidad_inmueble} onChange={this.handleChange}>
                      <option>Alta</option>
                      <option>Media</option>
                      <option>Baja</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Acorde con el entorno</label>
            </div>
            <div className="field-body">
              <div className="field is-narrow">
                <div className="control is-expanded">
                  <div className="select is-fullwidth is-small">
                    <select name="acorde_con_entorno" value={this.state.acorde_con_entorno} onChange={this.handleChange}>
                      <option>Si</option>
                      <option>No</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Habilitada</label>
            </div>
            <div className="field-body">
              <div className="field is-narrow">
                <div className="control is-expanded">
                  <div className="select is-fullwidth is-small">
                    <select name="habilitada" value={this.state.habilitada} onChange={this.handleChange}>
                      <option>Si</option>
                      <option>No</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Edad aparente</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <div className="control">
                  <input name="edad_aparente" value={this.state.edad_aparente} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                </div>
                <p className="control">
                  <a className="button is-small is-static">
                    Año(s)
                  </a>
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Vida útil estimada</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <div className="control">
                  <input name="vida_util_estimada" value={this.state.vida_util_estimada} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                </div>
                <p className="control">
                  <a className="button is-small is-static">
                    Año(s)
                  </a>
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Coeficiente (K)</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <div className="control">
                  <input name="coeficiente" value={this.state.coeficiente} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">% de vida</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <div className="control">
                  <input name="porciento_de_vida" readOnly value={this.porciento_de_vida()} className="input is-small" type="text" placeholder="" />
                </div>
                <p className="control">
                  <a className="button is-small is-static">
                    %
                  </a>
                </p>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-small">
              <label className="label">Edad útil remanente</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <div className="control">
                  <input name="edad_util_remanente" value={this.state.edad_util_remanente} onChange={this.handleChange} className="input is-small" type="text" placeholder="" />
                </div>
                <p className="control">
                  <a className="button is-small is-static">
                    Año(s)
                  </a>
                </p>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <h2 className="title is-4">
            Valor de edificación
          </h2>
          <label className="label is-small">Distribución</label>
          <div className="field is-grouped">
            <p className="control">
              <a className="button is-small is-info" onClick={() => this.addElement(this.state.distribucion, 'distribucion')}>
                <i className="fas fa-plus"/>
              </a>
            </p>
          </div>

          {this.state.distribucion[0] ? this.state.distribucion.map((a, i)=>{
            return (
              <Step6Mod
                key={i}
                number={i + 1}
                data={a.data}
                id={a.id}
                add={(d) => this.addDataToElement(d, this.state.distribucion, 'distribucion')}
                removeElement={(e) => this.removeElement(e, this.state.distribucion, 'distribucion')}
              />
            )
          }):[]}

          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label is-small">Construcción</label>
                <div className="control">
                  <textarea name="construccion" defaultValue={this.state.construccion} onChange={this.handleChange} className="textarea is-small" placeholder="Textarea"/>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="field">
                <label className="label is-small">Terminación</label>
                <div className="control">
                  <textarea name="terminacion" defaultValue={this.state.terminacion} onChange={this.handleChange}className="textarea is-small" placeholder="Textarea"/>
                </div>
              </div>
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label is-small">Cant.</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control is-expanded">
                      <input name="cantidad" value={String(this.state.cantidad).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </p>
                    <p className="control">
                      <span className="select is-small">
                        <select name="cantidad_count" value={this.state.cantidad_count} onChange={this.handleChange}>
                          <option>m²</option>
                          <option>m</option>
                        </select>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="field">
                <label className="label is-small">VUP</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <span className="select is-small">
                        <select name="vup" value={this.state.vup} onChange={this.handleChange}>
                          <option>RD$</option>
                          <option>US$</option>
                        </select>
                      </span>
                    </p>
                    <p className="control is-expanded">
                      <input name="vup_value" value={String(this.state.vup_value).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} onChange={this.handleChange} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="field">
                <label className="label is-small has-text-danger">(%) Dep. Ross-Heidecke</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        %
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="porcentaje_drh" value={this.state.porcentaje_drh} onChange={this.handleChange} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="field">
                <label className="label is-small">V. U. Dep</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="vu_dep" readOnly value={String(this.vu_dep()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="field">
                <label className="label is-small">S-Total</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="stotal" readOnly value={String(this.stotal()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Valor total Inmueble</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-normal is-static">
                    RD$
                  </a>
                </p>
                <div className="control is-expanded">
                  <input name="valor_total_ajustado" readOnly value={String(this.stotal()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-normal" type="text" placeholder=""/>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <h2 className="title is-4">
            Otras mejoras
          </h2>

          <div className="field is-grouped">
            <p className="control">
              <a className="button is-small is-info" onClick={() => this.addElement(this.state.otras_mejoras_data, 'otras_mejoras_data')}>
                <i className="fas fa-plus"/>
              </a>
            </p>
          </div>
          {this.state.otras_mejoras_data[0] ? this.state.otras_mejoras_data.map((a, i) => {
              return(
                <Step6Module
                  key={i}
                  number={i}
                  data={a.data}
                  id={a.id}
                  add={(d) => this.addDataToElement(d, this.state.otras_mejoras_data, 'otras_mejoras_data')}
                  removeElement={(e) => this.removeElement(e, this.state.otras_mejoras_data, 'otras_mejoras_data')}
                />)
          }) : []}

          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Valor total Otras Mejoras</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-normal is-static">
                    RD$
                  </a>
                </p>
                <div className="control is-expanded">
                  <input name="valor_total_ajustado" readOnly value={String(this.valor_total()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-normal" type="text" placeholder=""/>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <h2 className="title is-4">
            Resumen de valores
          </h2>
          <h2 className="title is-5 has-text-weight-light	has-text-centered">
            Terrenos
          </h2>
          <div className="columns">
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Valor Total</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valor_total_a" readOnly value={String(this.valor_total_ajustado_terrenos()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small has-text-danger">Otros ajustes</label>
                <div className="field-body">
                  <div className="field">
                    <p className="control is-expanded">
                      <input name="otros_ajustes_a" value={this.state.otros_ajustes_a} onChange={this.handleChange} className="input is-small has-text-danger" type="text" placeholder=""/>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small has-text-danger">%</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control is-expanded">
                      <input name="porcentaje_a" value={this.state.porcentaje_a} onChange={this.handleChange} className="input is-small has-text-danger" type="text" placeholder="" />
                    </p>
                    <p className="control">
                      <a className="button is-small is-static has-text-danger">
                        %
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Valor total ajustado</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valor_total_ajustado_a" readOnly value={String(this.valor_total_ajustado_a()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h2 className="title is-5 has-text-weight-light	has-text-centered">
            Edificaciones
          </h2>
          <div className="columns">
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Valor Total</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valor_total_b" readOnly value={String(this.stotal()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small has-text-danger">Otros ajustes</label>
                <div className="field-body">
                  <div className="field">
                    <p className="control is-expanded">
                      <input name="otros_ajustes_b" value={this.state.otros_ajustes_b} onChange={this.handleChange} className="input is-small has-text-danger" type="text" placeholder=""/>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small has-text-danger">%</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control is-expanded">
                      <input name="porcentaje_b" value={this.state.porcentaje_b} onChange={this.handleChange} className="input is-small has-text-danger" type="text" placeholder="" />
                    </p>
                    <p className="control">
                      <a className="button is-small is-static has-text-danger">
                        %
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Valor total ajustado</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valor_total_ajustado_b" readOnly value={String(this.valor_total_ajustado_b()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h2 className="title is-5 has-text-weight-light	has-text-centered">
            Otras mejoras
          </h2>
          <div className="columns">
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Valor Total</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valor_total_x" readOnly value={String(this.valor_total()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small has-text-danger">Otros ajustes</label>
                <div className="field-body">
                  <div className="field">
                    <p className="control is-expanded">
                      <input name="otros_ajustes_x" value={this.state.otros_ajustes_x} onChange={this.handleChange} className="input is-small has-text-danger" type="text" placeholder=""/>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small has-text-danger">%</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control is-expanded">
                      <input name="porcentaje_x" value={this.state.porcentaje_x} onChange={this.handleChange} className="input is-small has-text-danger" type="text" placeholder="" />
                    </p>
                    <p className="control">
                      <a className="button is-small is-static has-text-danger">
                        %
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Valor total ajustado</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valor_total_ajustado_x" readOnly value={String(this.valor_total_ajustado_x()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Valor total inmueble</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-normal is-static">
                    RD$
                  </a>
                </p>
                <div className="control is-expanded">
                  <input name="valor_total_inmueble" readOnly value={String(this.valor_total_inmueble()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-normal" type="text" placeholder="" />
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Valor total ajustado</label>
            </div>
            <div className="field-body">
              <div className="field has-addons">
                <p className="control">
                  <a className="button is-normal is-static">
                    RD$
                  </a>
                </p>
                <div className="control is-expanded">
                  <input name="valor_total_ajustado_input" value={this.state.valor_total_ajustado_input} onChange={this.handleChange} className="input is-normal" type="text" placeholder=""/>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Valor en texto</label>
            </div>
            <div className="field-body">
              <div className="field">
                <div className="control">
                  <input name="valor_en_texto" value={this.state.valor_en_texto} onChange={this.handleChange} className="input is-normal" type="text" placeholder=""/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
