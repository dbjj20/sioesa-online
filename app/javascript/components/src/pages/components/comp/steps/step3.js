import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'

export default class Step3 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      asfaltada: 'Si',
      aceras: 'Si',
      contenes: 'Si',
      area_verde: 'Si',
      area_deportiva: 'Próximo',
      dist_a_la_via_principal: 'Próximo',
      drenaje_sanitario: 'Si',
      drenaje_pluvial: 'Si',
      agua_potable: 'Si',
      alumbrado_publico: 'Si',
      linea_telefono: 'Si',
      cable: '',
      est_alta_tension_bco_transformadores: 'Próximo',
      transporte_publico: 'Próximo',
      centros_salud: 'Próximo',
      centros_educativos: 'Próximo',
      of_publicas: 'Próximo',
      comercios_menores: 'Próximo',
      est_gasolina: 'Próximo',
      est_glp: 'Próximo',
      created_at: '',
      updated_at: '',
      proyecto_id: Number,
      principales: '',
      secundarias: '',
      lugares_sectores_comunidades_cercanas: '',
    }
    //servicios_disponibles, vias_acceso
    this.handleChange = this.handleChange.bind(this)

  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 3 EDITING ----->', data)

      await this.setState(getPreparedProperties(['servicios_disponibles','vias_acceso'], data))
      console.log(this.state)
    }
  }

  render(){
    return(
      <div>
        <div className="container min-height">
          <h2 className="title is-3 has-text-weight-light">
            Servicios disponible en el sector y/o zona
          </h2>
          <h3 className="subtitle is-6">{`Esc: Lejano > 2 km, Cercano < 2 km y > 1 km y Proximo < 1 km `}</h3>
          <div className="divider"/>
          <div className="columns">
              <div className="column is-two-thirds is-offset-1">
                <h2 className="title is-5 has-text-weight-light	has-text-centered">
                  Vías y áreas públicas
                </h2>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Asfaltada</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="asfaltada" value={this.state.asfaltada} onChange={this.handleChange}>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                            <option value="Parcial">Parcial</option>
                            <option value="Disponible">Disponible</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Aceras</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="aceras" value={this.state.aceras} onChange={this.handleChange}>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                            <option value="Parcial">Parcial</option>
                            <option value="Disponible">Disponible</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Contenes</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="contenes" value={this.state.contenes} onChange={this.handleChange}>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                            <option value="Parcial">Parcial</option>
                            <option value="Disponible">Disponible</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Área verde</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="area_verde" value={this.state.area_verde} onChange={this.handleChange}>
                            <option value="Si">Si</option>
                            <option value="No">No</option>
                            <option value="Parcial">Parcial</option>
                            <option value="Disponible">Disponible</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Área Deportivas</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="area_deportiva" value={this.state.area_deportiva} onChange={this.handleChange}>
                            <option value="Próximo">Próximo</option>
                            <option value="Cercano">Cercano</option>
                            <option value="Lejano">Lejano</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Dist. a la vía principal</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control is-expanded">
                        <div className="select is-fullwidth is-small">
                          <select name="dist_a_la_via_principal" value={this.state.dist_a_la_via_principal} onChange={this.handleChange}>
                            <option value="Próximo">Próximo</option>
                            <option value="Cercano">Cercano</option>
                            <option value="Lejano">Lejano</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div className="divider"/>
          <div className="columns">
            <div className="column is-two-thirds is-offset-1">
              <h2 className="title is-5 has-text-weight-light	has-text-centered">
                Servicios básicos
              </h2>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Drenaje sanitario</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="drenaje_sanitario" value={this.state.drenaje_sanitario} onChange={this.handleChange}>
                          <option value="Si">Si</option>
                          <option value="No">No</option>
                          <option value="Parcial">Parcial</option>
                          <option value="Disponible">Disponible</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Drenaje pluvial</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="drenaje_pluvial" value={this.state.drenaje_pluvial} onChange={this.handleChange}>
                          <option value="Si">Si</option>
                          <option value="No">No</option>
                          <option value="Parcial">Parcial</option>
                          <option value="Disponible">Disponible</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Agua potable</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="agua_potable" value={this.state.agua_potable} onChange={this.handleChange}>
                          <option value="Si">Si</option>
                          <option value="No">No</option>
                          <option value="Parcial">Parcial</option>
                          <option value="Disponible">Disponible</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Alumbrado público</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="alumbrado_publico" value={this.state.alumbrado_publico} onChange={this.handleChange}>
                          <option value="Si">Si</option>
                          <option value="No">No</option>
                          <option value="Parcial">Parcial</option>
                          <option value="Disponible">Disponible</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Linea de telefono y cable</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="linea_telefono" value={this.state.linea_telefono} onChange={this.handleChange}>
                          <option value="Si">Si</option>
                          <option value="No">No</option>
                          <option value="Parcial">Parcial</option>
                          <option value="Disponible">Disponible</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Est. de alta tensión y/o Bco. transformadores</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="est_alta_tension_bco_transformadores" value={this.state.est_alta_tension_bco_transformadores} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <div className="columns">
            <div className="column is-two-thirds is-offset-1">
              <h2 className="title is-5 has-text-weight-light	has-text-centered">
                Otros servicios
              </h2>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Transporte público</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="transporte_publico" value={this.state.transporte_publico} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Centros de salud</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="centros_salud" value={this.state.centros_salud} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Centros educativos</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="centros_educativos" value={this.state.centros_educativos} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Oficinas Públicas</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="of_publicas" value={this.state.of_publicas} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Comercios menores</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="comercios_menores" value={this.state.comercios_menores} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Est. de gasolina</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="est_gasolina" value={this.state.est_gasolina} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Est. de GLP</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="est_glp" value={this.state.est_glp} onChange={this.handleChange}>
                          <option value="Próximo">Próximo</option>
                          <option value="Cercano">Cercano</option>
                          <option value="Lejano">Lejano</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"></div>
          <div className="columns">
            <div className="column is-two-thirds is-offset-1">
              <h2 className="title is-5 has-text-weight-light	has-text-centered">
                Vías de acceso
              </h2>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Principales</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <div className="control">
                      <input name="principales" value={this.state.principales} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Secundarias</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <div className="control">
                      <input name="secundarias" value={this.state.secundarias} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div className="divider"/>
          <div className="columns">
            <div className="column is-two-thirds is-offset-1">
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Lugares, sectores y comunidades cercanas y/o colindantes</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <div className="control">
                      <textarea name="lugares_sectores_comunidades_cercanas" defaultValue={this.state.lugares_sectores_comunidades_cercanas} onChange={this.handleChange} className="textarea is-small" placeholder=""/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
