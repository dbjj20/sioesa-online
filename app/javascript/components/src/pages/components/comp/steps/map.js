import React from 'react'
import GoogleMapReact from 'google-map-react';
const AnyReactComponent = ({ text }) => <div>{text}</div>;

export default class modal extends React.Component{
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  }
  constructor(props){
    super(props)
    this.state = {
      lat: 18.904254,
      lng: -70.240732414,
      zoom: 11
    }
    this.handleChange = this.handleChange.bind(this)
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    await this.setState(this.props.data)
    console.log(this.state)
  }

  render(){
    return(
      <article className="panel">
        <div className="panel-block">
          <div style={{ height: '60vh', width: '100%' }}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyAWbyybwJeDWABvpYKiNmi-uo2UyufDf14' }}
              center={{lat: this.state.lat, lng: this.state.lng}}
              defaultZoom={this.props.data.zoom}
              onClick={async (m) => {await this.setState({lat: m.lat, lng: m.lng})}}
              onChange={async (a) => {await this.setState({zoom: a.zoom})}}
            >
              <AnyReactComponent
                lat={this.state.lat}
                lng={this.state.lng}
                text="📍"
              />
            </GoogleMapReact>
          </div>
        </div>
    </article>
    )
  }
}
