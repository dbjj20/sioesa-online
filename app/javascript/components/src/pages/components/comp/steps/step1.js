import React from 'react'
import Modal from '../modal'
import map from './map'
import DatePicker, { registerLocale } from "react-datepicker";
import Step1Mod from './step1_mod'
import es from 'date-fns/locale/es';
import keygen from 'keygen'
import { getPreparedProperties } from '../../../lib/utils'
registerLocale('es', es)

export default class Step1 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      project_name:'',
      determinacion: '',
      informe_es_valido: '',
      presentado: "Garantia hipotecaria",
      fecha_informe: new Date(),
      fecha_inspeccion: new Date(),
      tipo_de_inmueble: '',
      nombre: '',
      cedula: '',
      telefono: '',
      email: '',
      propietario: '',
      nombre_propietario: '',
      cedula_propietario: '',
      telefono_propietario: '',
      email_propietario: '',
      requerido_por_institucion: '',
      requerido_por_rnc: '',
      requerido_por_sucursal: '',
      requerido_por_gerente: '',
      requerido_por_telefono: '',
      requerido_por_email: '',
      libro: '',
      folio: '',
      matricula_select_label: "Constancia de titulo",
      matricula_select: '',
      designacion_castral: '',
      municipio_no: "1",
      municipio_name: '',
      provincia_no: "1",
      provincia_name: '',
      att: '',
      att_count: "m²",
      direccion_del_inmueble: '',
      coordenadas_inmueble: '',
      latitud_norte: 18.904254,
      longitud_oeste: -70.240732414,
      zoom: 11,
      norte: '',
      este: '',
      sur: '',
      oeste: '',
      created_at: new Date(),
      updated_at: new Date(),
      created_by_detail: '',
      updated_by_detail: '',
      modal: false,
      libro_folios: [],
      status: '',
      porcentaje: '',
      data: {}
    }
    //db obj, validacion, descripcion_legal, descripcion_inmueble, colindancias
    this.handleChange = this.handleChange.bind(this)
    this.addDataToElement = this.addDataToElement.bind(this)
    this.removeElement = this.removeElement.bind(this)
    this.addElement = this.addElement.bind(this)
  }

  async addDataToElement(obj, arr, name){
    let state = obj.state
    let provitional = []
    let current = {}
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id === obj.id){
        Object.assign(current, a)
      }
      console.log(a)
    })
    current.data = state

    provitional.map((a, i) => {
      if (a.id !== obj.id){
        result.push(a)
      }
      console.log(a)
    })

    result.push(current)
    console.log(result)
    await this.setState({[name]: result})

    console.log(obj, provitional)
    console.log(current)
  }

  async removeElement(id, arr, name){
    // await this.setState({[id]: "no"})
    let provitional = []
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id !== id){
        result.push(a)
      }
      console.log(a)
    })
    console.log(result)
    await this.setState({[name]: result})
  }

  async addElement(arr, name){
    let provitional = []
    Object.assign(provitional, arr)

    const id = keygen.hex(keygen.small)
    await this.setState({[id]: ''})
    provitional.push({key: "", value: "", id: id, data:{} })
    console.log(provitional)
    await this.setState({[name]: provitional})
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 1 EDITING ----->', data)

      await this.setState(getPreparedProperties(this.props.step1Keys, data))
      console.log(this.state)
      if (!this.state.libro_folios[0]){
        this.addElement(this.state.libro_folios, 'libro_folios')
      }
    }
    // debugger
  }

  render(){
    return(
      <div>
          <div className="container min-height" >
            <div className="field is-horizontal" onMouseOut={this.props.save}>
              <div className="field-label is-small">
                <label className="label">Nombre del proyecto</label>
              </div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input name="project_name" onChange={this.handleChange} value={this.state.project_name} className="input is-small" type="text" placeholder=""/>
                  </div>
                </div>
              </div>
            </div>
            <h2 className="title is-3 has-text-weight-light	">
              Descripción legal
            </h2>
            <div className="divider"/>
            <div className="field is-horizontal">
              <div className="field-label">
                <label className="label">Tasación para la la determinacion del</label>
              </div>
              <div className="field-body">
                <div className="field is-narrow">
                  <div className="control">
                    <div className="select is-fullwidth">
                      <select name="determinacion" onChange={this.handleChange} value={this.state.determinacion}>
                        <option value="Valor de mercado">Valor de mercado</option>
                        <option value="Valor de renta">Valor de renta</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="field is-horizontal">
              <div className="field-label">
                <label className="label">Este informe es válido</label>
              </div>
              <div className="field-body">
                <div className="field is-narrow">
                  <div className="control">
                    <label className="radio">
                      <input type="radio" name="informe_es_valido" value="Si" checked={this.state.informe_es_valido === "Si"} onChange={this.handleChange}/>
                      Si
                    </label>
                    <label className="radio">
                      <input type="radio" name="informe_es_valido" value="No" checked={this.state.informe_es_valido === "No"} onChange={this.handleChange}/>
                      No
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="field is-horizontal">
              <div className="field-label">
                <label className="label">para ser presentado como</label>
              </div>
              <div className="field-body">
                <div className="field is-narrow">
                  <div className="control">
                    <div className="select is-fullwidth">
                      <select name="presentado" onMouseOut={this.props.save} onChange={this.handleChange} value={this.state.presentado}>
                        <option value="Garantia hipotecaria">Garantia hipotecaria</option>
                        <option value="Para tribunal alguno">Para tribunal alguno</option>
                        <option value="Peritaje ante tribunal o corte">Peritaje ante tribunal o corte</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is-offset-1">
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Fecha informe</label>
                  </div>
                  <div className="field-body" onMouseOut={this.props.save}>
                    <div className="field">
                        <DatePicker
                          locale='es'
                          selected={new Date(this.state.fecha_informe)}
                          onChange={(date) => this.setState({fecha_informe: date})}
                        />
                      {/*
                        <div className="control">
                          <input name="fecha_informe" className="input is-small" type="text" placeholder="DD/MM/AA"/>
                        </div>
                      */}
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Fecha inspección</label>
                  </div>
                  <div className="field-body" onMouseOut={this.props.save}>
                    <div className="field">
                      <DatePicker
                        locale='es'
                        selected={new Date(this.state.fecha_inspeccion)}
                        onChange={(date) => this.setState({fecha_inspeccion: date})}
                      />
                      {/*
                        <div className="control">
                          <input name="fecha_inspeccion" className="input is-small" type="text" placeholder="DD/MM/AA"/>
                        </div>
                      */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is is-offset-1">
                <h2 className="title is-5 has-text-weight-light	has-text-centered">
                  Solicitante
                </h2>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Nombre</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="nombre" onChange={this.handleChange} value={this.state.nombre} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Cédula/RNC </label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="cedula" onChange={this.handleChange} value={this.state.cedula} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Teléfono</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="telefono" onMouseOut={this.props.save} onChange={this.handleChange} value={this.state.telefono} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Email</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="email" onChange={this.handleChange} value={this.state.email} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Propietario</label>
                  </div>
                  <div className="field-body">
                    <div className="field is-narrow">
                      <div className="control">
                        <label className="radio">
                          <input name="propietario" onChange={this.handleChange} value="Si" checked={this.state.propietario === "Si"} type="radio" />
                          Si
                        </label>
                        <label className="radio">
                          <input name="propietario"  onChange={this.handleChange} value="No" checked={this.state.propietario === "No"} type="radio"/>
                          No
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is is-offset-1">
                <h2 className="title is-5 has-text-weight-light	has-text-centered">
                  Propietario
                </h2>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Nombre</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="nombre_propietario" onChange={this.handleChange} value={this.state.nombre_propietario} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Cédula/RNC </label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="cedula_propietario" onMouseOut={this.props.save} onChange={this.handleChange} value={this.state.cedula_propietario} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Teléfono</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="telefono_propietario" onChange={this.handleChange} value={this.state.telefono_propietario} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Email</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="email_propietario" onChange={this.handleChange} value={this.state.email_propietario} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is is-offset-1">
                <h2 className="title is-5 has-text-weight-light	has-text-centered">
                  Requerido por
                </h2>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Institución</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="requerido_por_institucion" onChange={this.handleChange} value={this.state.requerido_por_institucion} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">RNC</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="requerido_por_rnc" onChange={this.handleChange} value={this.state.requerido_por_rnc} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Sucursal</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="requerido_por_sucursal" onChange={this.handleChange} value={this.state.requerido_por_sucursal} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Gerente</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="requerido_por_gerente" onChange={this.handleChange} value={this.state.requerido_por_gerente} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Teléfono</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="requerido_por_telefono" onChange={this.handleChange} value={this.state.requerido_por_telefono} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Email</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="requerido_por_email" onChange={this.handleChange} value={this.state.requerido_por_email} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="divider"/>

            <div className="columns">
              <div className="column is-two-thirds is is-offset-1">
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Tipo de inmueble</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="tipo_de_inmueble" onChange={this.handleChange} value={this.state.tipo_de_inmueble} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Libros / Folios</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <p className="control" onMouseOut={this.props.save}>
                        <a className="button is-small is-info" onClick={() => this.addElement(this.state.libro_folios, 'libro_folios')}>
                          <i className="fas fa-plus"/>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>

                {this.state.libro_folios[0] ? this.state.libro_folios.map((a, i) => {
                  return(
                    <Step1Mod
                      key={i}
                      number={i + 1}
                      data={a.data}
                      id={a.id}
                      add={(d) => this.addDataToElement(d, this.state.libro_folios, 'libro_folios')}
                      removeElement={(e) => this.removeElement(e, this.state.libro_folios, 'libro_folios')}
                    />
                  )
                }): []}
                <div className="field is-horizontal">
                  <div className="field-label">
                    <label className="label">
                        <span className="select is-small">
                          <select name="matricula_select_label" onMouseOut={this.props.save} onChange={this.handleChange}>
                            <option value="Constancia de titulo">Constancia de titulo</option>
                            <option value="Certificado de titulo">Certificado de titulo</option>
                            <option value="Matricula">Matrícula</option>
                          </select>
                        </span>
                    </label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control" onMouseOut={this.props.save}>
                        <input name="matricula_select" onChange={this.handleChange} value={this.state.matricula_select} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Designación catastral</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="designacion_castral" onChange={this.handleChange} value={this.state.designacion_castral} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Provincia</label>
                  </div>
                  <div className="field-body">
                    <div className="field has-addons">
                      <p className="control">
                        <span className="select is-small">
                          <select name="provincia_no" onMouseOut={this.props.save} value={this.state.provincia_no} onChange={this.handleChange} >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                          </select>
                        </span>
                      </p>
                      <p className="control is-expanded">
                        <input name="provincia_name" onChange={this.handleChange} value={this.state.provincia_name} className="input is-small" type="text" placeholder=""/>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Municipio</label>
                  </div>
                  <div className="field-body">
                    <div className="field has-addons">
                      <p className="control">
                        <span className="select is-small">
                          <select name="municipio_no" value={this.state.municipio_no} onChange={this.handleChange}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                          </select>
                        </span>
                      </p>
                      <p className="control is-expanded">
                        <input name="municipio_name" onChange={this.handleChange} value={this.state.municipio_name} className="input is-small" type="text" placeholder=""/>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Área total titulada</label>
                  </div>
                  <div className="field-body">
                    <div className="field has-addons">
                      <p className="control is-expanded">
                        <input name="att" onChange={this.handleChange} value={this.state.att} className="input is-small" type="text" placeholder=""/>
                      </p>
                      <p className="control">
                        <span className="select is-small">
                          <select name="att_count" value={this.state.att_count} onChange={this.handleChange}>
                            <option value="m²">m²</option>
                            <option value="m">m</option>
                          </select>
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Dirección del inmueble</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <textarea name="direccion_del_inmueble" onChange={this.handleChange} value={this.state.direccion_del_inmueble} className="textarea is-small" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is is-offset-1">
                <h2 className="title is-5 has-text-weight-light	has-text-centered">
                  Coordenadas geograficas del inmueble
                </h2>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Latitud norte</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="latitud_norte" onChange={this.handleChange} value={this.state.latitud_norte} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Longitud oeste</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="longitud_oeste" onChange={this.handleChange} value={this.state.longitud_oeste} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="buttons is-right">
                <button className="button is-small" onClick={async () =>{ await this.setState({modal: !this.state.modal})}}>
                  <span className="icon is-small">
                    <i className="fas fa-map-marker-alt"/>
                  </span>
                  <span>Ver en el mapa</span>
                </button>
              </div>
              </div>
            </div>
            <div className="divider"/>
            <div className="columns">
              <div className="column is-two-thirds is is-offset-1">
                <h2 className="title is-5 has-text-weight-light has-text-centered	">
                  Colindancias generales del lote
                </h2>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Norte</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="norte" onChange={this.handleChange} value={this.state.norte} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Este</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="este" onChange={this.handleChange} value={this.state.este} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Sur</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="sur" onChange={this.handleChange} value={this.state.sur} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field is-horizontal">
                  <div className="field-label is-small">
                    <label className="label">Oeste</label>
                  </div>
                  <div className="field-body">
                    <div className="field">
                      <div className="control">
                        <input name="oeste" onChange={this.handleChange} value={this.state.oeste} className="input is-small" type="text" placeholder=""/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {this.state.modal ? (
            <Modal
              title="Confirmar ubicación"
              saveButtonTitle="Guardar cambios"
              cancelButtonTitle="Cancelar"
              onClose={async () => { await this.setState({modal: !this.state.modal})}}
              onSave={async (d) => { await this.setState({latitud_norte: d.state.lat, longitud_oeste: d.state.lng, zoom: d.state.zoom, modal: !this.state.modal})}}
              data={{lat: this.state.latitud_norte, lng: this.state.longitud_oeste, zoom: this.state.zoom}}
              Child={map}
            />):[]}
      </div>
    )
  }
}
