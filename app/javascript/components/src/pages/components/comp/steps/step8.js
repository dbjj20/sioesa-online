import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'
import keygen from 'keygen'

export default class Step8 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      data: [],
      imageModal:false,
      current: ''
    }

    this.handleChangeDocument = this.handleChangeDocument.bind(this)
    this.addDataToElement = this.addDataToElement.bind(this)
  }


  async addDataToElement(obj, arr, name){
    let state = obj
    let provitional = []
    let current = {}
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id === obj.id){
        Object.assign(current, a)
      }
      console.log(a)
    })
    current = state

    provitional.map((a, i) => {
      if (a.id !== obj.id){
        result.push(a)
      }
      console.log(a)
    })

    result.push(current)
    console.log(result)
    await this.setState({[name]: result})

    console.log(obj, provitional)
    console.log(current)

  }

  async removeElement(id, arr, name){
    // await this.setState({[id]: "no"})
    let provitional = []
    let result = []

    Object.assign(provitional, arr)
    provitional.map((a, i) => {
      if (a.id !== id){
        result.push(a)
      }
      console.log(a)
    })
    console.log(result)
    await this.setState({[name]: result})
  }

  async addElement(arr, name, data){
    let provitional = []
    Object.assign(provitional, arr)

    const id = keygen.hex(keygen.small)
    await this.setState({[id]: ''})
    provitional.push({key: "", value: "", id: id, data: data || {}, active: true })
    console.log(provitional)
    await this.setState({[name]: provitional})
  }

  async handleChangeDocument(e){
    if (this.state.backup_path && this.state.project_name){
      let local_path = this.state.backup_path
      let im = []
        if (true){ //!fs.existsSync(`${local_path}/${this.state.project_name}/documentos/`)
          console.log("CREATING PATH")
          // fs.mkdirSync(`${local_path}/${this.state.project_name}/documentos/`, { recursive: true })
        }

        for (let f of e.target.files) {
          console.log('File(s) you dragged here: ', f.path)
          console.log(f)
          let extension = f.type.split('/')
          console.log(extension)
          if (extension[0] === 'image'){

            let name = `img-${keygen.hex()}.${extension[1]}`
            let img = `${local_path}/${this.state.project_name}/documentos/${name}`
            // fs.copyFile(f.path, img, (err) => {
            //   if (err) throw err;
            // })
            let obj = {name: f.name, original_path: f.path, type: f.type, size: f.size, save_path: img, formated_name: name}
            if (this.state.data[0]){
              this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: obj || {} }, this.state.data, 'data')
            }else {
              this.addElement(this.state.data, 'data', obj)
            }
          }else if (extension[0].match('application') && extension[1].match('pdf')) {

            let name = `pdf-${keygen.hex()}.${extension[1]}`
            let pdf = `${local_path}/${this.state.project_name}/documentos/${name}`
            // fs.copyFile(f.path, pdf, (err) => {
            //   if (err) throw err;
            // })
            let obj = {name: f.name, original_path: f.path, type: f.type, size: f.size, save_path: pdf, formated_name: name}
            if (this.state.data[0]){
              const pdf_file = f
              const callback = images => {
                  // the function returns an array
                  // every img is a normal file object
                  images.forEach(async img => {
                      console.log(img)
                      const buffer = Buffer.from(await new Response(img).arrayBuffer())
                      // fs.writeFile(`${local_path}/${this.state.project_name}/documentos/${pdf_file.name}--${img.name}`, buffer, function (err) {
                      //   if (err) throw err;
                      //   console.log('Saved!');
                      // })
                      this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: {
                        name: `${pdf_file.name}--${img.name}`,
                        type: 'image',
                        save_path: `${local_path}/${this.state.project_name}/documentos/${pdf_file.name}--${img.name}`
                      } }, this.state.data, 'data')
                  })
              }
              // convertPdfToPng(pdf_file, {
              //     outputType: 'callback',
              //     callback: callback
              // })
              // this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: obj || {} }, this.state.data, 'data')
            }else {
              const pdf_file = f
              const callback = images => {
                  // the function returns an array
                  // every img is a normal file object
                  images.forEach(async img => {
                      console.log(img)
                      const buffer = Buffer.from(await new Response(img).arrayBuffer())
                      // fs.writeFile(`${local_path}/${this.state.project_name}/documentos/${pdf_file.name}--${img.name}`, buffer, function (err) {
                      //   if (err) throw err;
                      //   console.log('Saved!');
                      // })
                      this.addDataToElement({key: "", value: "", id: keygen.hex(keygen.small), data: {
                        name: `${pdf_file.name}--${img.name}`,
                        type: 'image',
                        save_path: `${local_path}/${this.state.project_name}/documentos/${pdf_file.name}--${img.name}`
                      } }, this.state.data, 'data')
                  })
              }
              // convertPdfToPng(pdf_file, {
              //     outputType: 'callback',
              //     callback: callback
              // })
            }

          }else{
            alert("Solo imagenes o archivos PDF")
          }
        }
    } else {
        alert("No hay un ruta definida de backup y/o nombre de proyecto")
    }
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 8 EDITING ----->', data)

      await this.setState(getPreparedProperties(['documentos', 'init'], data))
      let p = getPreparedProperties(['proyecto'], data)
      p.p_data = p.data
      delete p.data
      await this.setState(p)
      console.log(this.state)
    }
  }

  render(){
    return(
      <div >
        <form className="container min-height">
          <h2 className="title is-4">
            Agregar documentos
          </h2>
          <div className="divider"/>
          <div className="field">
            <div className="file is-medium is-boxed is-centered">
              <label className="file-label">
                <input className="button" id="drag" name='foto' type="file" accept="*" multiple={true} onDrop={this.handleChangeDocument} onChange={this.handleChangeDocument} placeholder="Subir o soltar documentos aquí" />
              </label>
            </div>
          </div>
          <div className="divider"/>
          <article className="panel">
            {this.state.data[0] ? this.state.data.map((d, i) => {
              return (
                <div key={i} className="panel-block">
                  <span className="panel-icon">
                    <i className={`far ${d.data.type.match('image') ? 'fa-file-image': 'fa-file-pdf'}`} />
                  </span>
                  <a onClick={async () => {await this.setState({imageModal: true, current: d.data.save_path})}}>{d.data.name}</a>

                  <a className="delete" onClick={() => this.removeElement(d.id, this.state.data, 'data')}/>
                  {this.state.imageModal ? (
                    <div className="modal is-active">
                      <div className="modal-background"/>
                      <div className="modal-card" style={{width: "80%", height: "100%"}}>
                        <header className="modal-card-head">
                          <button className="delete is-medium" aria-label="close" onClick={async() => {await this.setState({imageModal: false, current: ''})}}/>
                        </header>
                        <section className="modal-card-body">
                          <img src={'file://' + this.state.current}/>
                        </section>
                      </div>
                    </div>
                    ):[]}
                </div>
              )
            }):[]}
          </article>
        </form>
      </div>
    )
  }
}
