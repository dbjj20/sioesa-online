import React from 'react'
import { getPreparedProperties } from '../../../lib/utils'

export default class Step4 extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      proyecto_id: Number,
      area_const_primer_nivel: '',
      area_const_primer_nivel_count: 'm²',
      long_frente: '',
      long_frente_count: '',
      long_fondo_prom: '',
      long_fondo_prom_count: 'ml',
      aocp: '',
      aocp_count: 'm²',
      const_solar: '',
      factor_frente_fondo: '',
      geometria: 'Regular',
      topografia: 'Ondulada',
      ubicacion_manzana: 'Medial',
      uso_actual: 'Comercial',
      deseabilidad: 'Alta',
      mejor_mas_alto_uso: 'Comercial',
      notas_extras: Array,
      created_at: '',
      updated_at: '',
    }
    this.handleChange = this.handleChange.bind(this)
    //caracteristicas
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      console.log('STEP 4 EDITING ----->', data)

      await this.setState(getPreparedProperties(['caracteristicas'], data))
      console.log(this.state)
    }
  }

  render(){
    return(
      <div >
        <div className="container min-height">
          <h2 className="title is-4">
            Caracteristicas del emplazamiento o solar
          </h2>
          <h3 className="subtitle is-6">Longitudes aproximadas levantadas con cinta y/o laser métrico de mano</h3>
          <div className="divider"></div>
          <div className="columns">
            <div className="column is-two-thirds is-offset-1">
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Área const. 1er nivel</label>
                </div>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <input name="area_const_primer_nivel" value={this.state.area_const_primer_nivel} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </p>
                    <p className="control">
                      <span className="select is-small">
                        <select name="area_const_primer_nivel_count" value={this.state.area_const_primer_nivel_count} onChange={this.handleChange}>
                          <option value="m²">m²</option>
                          <option value="m">m</option>
                        </select>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Long. frente</label>
                </div>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <input name="long_frente" value={this.state.long_frente} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </p>
                    <p className="control">
                      <span className="select is-small">
                        <select name="long_frente_count" value={this.state.long_frente_count} onChange={this.handleChange}>
                          <option value="ml">ml</option>
                          <option value="m²">m²</option>
                        </select>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Long. fondo prom.</label>
                </div>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <input name="long_fondo_prom" value={this.state.long_fondo_prom} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </p>
                    <p className="control">
                      <span className="select is-small">
                        <select name="long_fondo_prom_count" value={this.state.long_fondo_prom_count} onChange={this.handleChange}>
                          <option value="ml">ml</option>
                          <option value="m²">m²</option>
                        </select>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">(AOCP) = Área ocupada</label>
                </div>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <input name="aocp" value={this.state.aocp} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </p>
                    <p className="control">
                      <span className="select is-small">
                        <select name="aocp_count" value={this.state.aocp_count} onChange={this.handleChange}>
                          <option value="m²">m²</option>
                          <option value="m">m</option>
                        </select>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">% Const. / solar</label>
                </div>
                <div className="field-body">
                  <div className="field has-addons">
                    <div className="control">
                      <input name="const_solar" value={this.state.const_solar} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </div>
                    <p className="control">
                      <a className="button is-small is-static">
                        %
                      </a>
                    </p>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Factor frente / fondo</label>
                </div>
                <div className="field-body">
                  <div className="field has-addons">
                    <div className="control">
                      <input name="factor_frente_fondo" value={this.state.factor_frente_fondo} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Geometría</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="geometria" value={this.state.geometria} onChange={this.handleChange}>
                          <option value="Regular">Regular</option>
                          <option value="Irregular">Irregular</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Topografía</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="topografia" value={this.state.topografia} onChange={this.handleChange}>
                          <option value="Ondulada">Ondulada</option>
                          <option value="Plana">Plana</option>
                          <option value="Pendiente">Pendiente fuerte</option>
                          <option value="Bajo">Bajo el nivel de la calle</option>
                          <option value="Montañoso">Montañoso</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Ubicación manzana</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="ubicacion_manzana" value={this.state.ubicacion_manzana} onChange={this.handleChange}>
                          <option value="Medial">Medial</option>
                          <option value="Esquina">Esquina</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Uso actual</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="uso_actual" value={this.state.uso_actual} onChange={this.handleChange}>
                          <option value="Comercial">Comercial</option>
                          <option value="Residencial">Residencial</option>
                          <option value="Mixto">Mixto</option>
                          <option value="Institucional">Institucional</option>
                          <option value="Agricola">Agricola</option>
                          <option value="Yermo">Yermo</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Mejor y más alto uso</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="mejor_mas_alto_uso" value={this.state.mejor_mas_alto_uso} onChange={this.handleChange}>
                          <option value="Comercial">Comercial</option>
                          <option value="Residencial">Residencial</option>
                          <option value="Mixto">Mixto</option>
                          <option value="Institucional">Institucional</option>
                          <option value="Industrial">Industrial</option>
                          <option value="Agricola">Agricola</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Deseabilidad</label>
                </div>
                <div className="field-body">
                  <div className="field is-narrow">
                    <div className="control is-expanded">
                      <div className="select is-fullwidth is-small">
                        <select name="deseabilidad" value={this.state.deseabilidad} onChange={this.handleChange}>
                          <option value="Alta">Alta</option>
                          <option value="Media">Media</option>
                          <option value="Baja">Baja</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"></div>
          <div className="columns">
            <div className="column is-two-thirds is is-offset-1">
              <div className="field is-horizontal">
                <div className="field-label is-small">
                  <label className="label">Notas extras</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <div className="control">
                      <textarea name="notas_extras" defaultValue={this.state.notas_extras} onChange={this.handleChange} className="textarea is-small" placeholder=""></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }
}
