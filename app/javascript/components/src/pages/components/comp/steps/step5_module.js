import React from 'react'

export default class Step5Module extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      valor: '',
      moneda: 'RD$',
      area: '',
      area_count: 'm²',
      vu: '0',
      vu_count: 'RD$/m²',
      especulacion: 0,
      por_dif_area: 0,
      ubicacion: 0,
      factor: 0,
      tipo_de_suelo: 0,
      topografia: 0,
      forma_geometrica: 0,
      factor_homogenizacion: '',
      vuh: '',
      vuh_count: 'RD$/m²',

      created_at: '',
      updated_at: '',
    }
    //ofertas_ventas_comparables, homogenizacion_comparables
    this.handleChange = this.handleChange.bind(this)
    this.save = this.save.bind(this)
    this.formatPriceInput = this.formatPriceInput.bind(this)
    this.factorHomogenizacion = this.factorHomogenizacion.bind(this)
    this.por_dif_area = this.por_dif_area.bind(this)
    this.vuh = this.vuh.bind(this)
  }

  vuh(){
    let vu = this.state.vu
    let factor_homogenizacion = this.factorHomogenizacion()
    let result = parseFloat(vu) * parseFloat(factor_homogenizacion)

    // await this.setState({vuh: result.toFixed(2)})
    return result.toFixed(2)
  }

  por_dif_area(){
    let result = parseFloat((parseFloat(this.state.area) / parseFloat(this.props.valorTerreno) ) ** 0.1)
    // await this.setState({por_dif_area: result})
    // debugger
    return result.toFixed(4)
  }

  factorHomogenizacion(){
    let {especulacion, ubicacion, factor, tipo_de_suelo, topografia, forma_geometrica} = this.state
    let por_dif_area = this.por_dif_area()
    let result = parseFloat(especulacion) * parseFloat(ubicacion) * parseFloat(factor) * parseFloat(tipo_de_suelo) * parseFloat(topografia) * parseFloat(forma_geometrica) * parseFloat(por_dif_area)
    // await this.setState({factor_homogenizacion: result.toFixed(4)})
    // debugger
    return result.toFixed(4)
  }

  formatPriceInput (s) {
    //using regrex to format the price input and get only numbers with commas
		if (typeof s !== 'string' || s === "undefined") return ''
		return s.toString().replace(/[^0-9.]/g, "")
	}

  async save(){
    await this.props.add({
      id: this.props.id,
      state: this.state
    })
    // await this.props.save()
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: this.formatPriceInput(value),
      vuh: this.vuh()
     })

    await this.props.add({
      id: this.props.id,
      state: this.state
    })
  }

  async componentDidMount(){
    console.log("DATA FROM PROPS MODULE STEP 5 =---->", this.props.data)
    await this.setState(this.props.data)
    await this.setState({vuh: this.vuh()})
    // await this.save()
  }

  render(){
    return(
      <div className="container">
        <div className="columns is-multiline">
          <h3 className="title column is-12">
            <span className="tag is-large">#{this.props.number + 1}</span>
            &nbsp;
            <a className="tag is-large is-info" onClick={this.save}>
              <i className="fas fa-save"/>
            </a>
          </h3>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Valor</label>
              <div className="field-body">
                <div className="field has-addons">
                  <p className="control is-expanded">
                    <input name="valor" className="input is-small" type="text" placeholder="" value={String(parseFloat(this.state.area * this.state.vu).toFixed(2)).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}/>
                  </p>
                  <p className="control">
                    <span className="select is-small">
                      <select name="moneda" value={this.state.moneda} onChange={this.handleChange}>
                        <option value="RD$">RD$</option>
                        <option value="US$">US$</option>
                      </select>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Área</label>
              <div className="field-body">
                <div className="field has-addons">
                  <p className="control is-expanded">
                    <input name="area" value={String(this.state.area).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                  <p className="control">
                    <span className="select is-small">
                      <select name="area_count" value={this.state.area_count} onChange={this.handleChange}>
                        <option value="m²">m²</option>
                        <option value="m">m</option>
                      </select>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Especulación</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="especulacion" value={this.state.especulacion} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Ubicación</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="ubicacion" value={this.state.ubicacion} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Factor (frente/fondo)</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="factor" value={this.state.factor} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Tipo de suelo</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="tipo_de_suelo" value={this.state.tipo_de_suelo} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Topografía</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="topografia" value={this.state.topografia} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Forma geometrica</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="forma_geometrica" value={this.state.forma_geometrica} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">V.U.</label>
              <div className="field-body">
                <div className="field has-addons">
                  <p className="control is-expanded">
                    <input name="vu" value={this.state.vu} onChange={this.handleChange} className="input is-small" type="text" placeholder="" />
                  </p>
                  <p className="control">
                    <span className="select is-small">
                      <select name="vu_count" value={this.state.vu_count} onChange={this.handleChange}>
                        <option value="RD$/m²">RD$/m²</option>
                        <option value="US$/m²">US$/m²</option>
                        <option value="RD$/m">RD$/m</option>
                        <option value="US$/m">US$/m</option>
                      </select>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Por dif. de área</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="por_dif_area" readOnly value={this.por_dif_area()} className="input is-small" type="text" placeholder="" />
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">Factor homogenización</label>
              <div className="field-body">
                <div className="field ">
                  <p className="control is-expanded">
                    <input name="factor_homogenizacion" readOnly value={this.factorHomogenizacion()} className="input is-small" type="text" placeholder="" />
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <div className="field">
              <label className="label is-small">V.U.H</label>
              <div className="field-body">
                <div className="field has-addons">
                  <p className="control is-expanded">
                    <input name="vuh" readOnly value={String(this.vuh()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                  </p>
                  <p className="control">
                    <span className="select is-small">
                      <select name="vuh_count" value={this.state.vuh_count} onChange={this.handleChange}>
                        <option value="RD$/m²">RD$/m²</option>
                        <option value="US$/m²">US$/m²</option>
                        <option value="RD$/m">RD$/m</option>
                        <option value="US$/m">US$/m</option>
                      </select>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="divider"/>
      </div>
    )
  }
}
