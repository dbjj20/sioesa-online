import React from 'react'
import { getPreparedProperties, calculateStep5 } from '../../../lib/utils'
import Step5Module from './step5_module'
import keygen from 'keygen'

export default class Step5 extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      proyecto_id: Number,
      data: [],
      promedio: 0.00,
      mediana: 0.00,
      variacion: 0.00,
      coeficienteV: 0.00,
      valorTerreno: 0.00,
      area_total: 0.00,
      created_at: '',
      updated_at: '',
    }
    this.addElement = this.addElement.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.removeElement = this.removeElement.bind(this)
    this.addDataToElement = this.addDataToElement.bind(this)
    //ofertas_ventas_comparables, homogenizacion_comparables
    // TODO: put in original index array values indexeses
  }

  async addDataToElement(obj){
    let input = this.state[obj.id]
    let state = obj.state
    let provitional = []
    let current = {}
    let result = []

    Object.assign(provitional, this.state.data)
    provitional.map((a, i) => {
      if (a.id === obj.id){
        Object.assign(current, a)
      }
    })
    current.data = state
    current.value = input

    provitional.map((a, i) => {
      if (a.id !== obj.id){
        result.push(a)
      }

    })

    result.push(current)
    await this.setState({data: result})
    await this.setState(calculateStep5(this.state.data))
    await this.props.save()
  }

  async removeElement(id){
    // await this.setState({[id]: "no"})
    let provitional = []
    let result = []

    Object.assign(provitional, this.state.data)
    provitional.map((a, i) => {
      if (a.id !== id){
        result.push(a)
      }
      console.log(a)
    })
    console.log(result)
    await this.setState({data: result})
    await this.setState(calculateStep5(result))
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async addElement(){
    let provitional = []
    Object.assign(provitional, this.state.data)

    const id = keygen.hex(keygen.small)
    await this.setState({[id]: ''})
    provitional.push({key: "", value: "", id: id, data:{} })
    await this.setState({data: provitional})

  }

  async componentDidMount(){
    if (this.props.db_data){
      let data = await this.props.db_data()
      if (data.ofertas_ventas_comparables){
        console.log('STEP 5 EDITING ----->', data)

        await this.setState(calculateStep5(data.ofertas_ventas_comparables.data))

        data.ofertas_ventas_comparables.data.map(async (d, i)=> {
          console.log(d)
          await this.setState({
            [d.id]: d.value
          })
        })

        await this.setState(getPreparedProperties(['ofertas_ventas_comparables'], data))
      }else {
        const id = keygen.hex(keygen.small)
        await this.setState({
          data: [{key: "", value: "", id: id, data:{} }],
          [id]: ''
        })
      }
    }
  }

  render(){
    return(
      <div>
        <div className="container min-height">
          <h2 className="title is-4">
            Análisis del valor comparativa
          </h2>
          <h3 className="subtitle is-6">Letreros expuestos en la via pública, ventas no realizadas, que no establecen un precio en la zona pero, si nos dan un estimado de los valores máximos ofertados por los dueños de las propiedades, que manejados con criterio profesional podemos determinar el valor máximo promedio del inmueble de interes</h3>
          <div className="divider"/>

          {this.state.data[0] ? this.state.data.map((o, i) => {
              return (
                <div className="field has-addons" key={i}>
                  <div className="field is-grouped">
                    <p className="control">
                      <a className="button is-small is-danger" onClick={() => this.removeElement(o.id)}>
                        <i className="fas fa-minus"/>
                      </a>
                    </p>
                  </div>
                  &nbsp;
                  <p className="control">
                    <a className="button is-small is-static">
                      #{i + 1}
                    </a>
                  </p>
                  <div className="control is-expanded">
                    <input name={o.id} value={this.state[o.id]} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
                  </div>
                </div>
              )
          }) :[]}

          <div className="field is-grouped">
            <p className="control">
              <a className="button is-small is-info" onClick={this.addElement}>
                <i className="fas fa-plus"/>
              </a>
            </p>
          </div>

          <div className="divider"/>

          <h2 className="title is-4">
            Homogenización de las comparables
          </h2>

          {this.state.data[0] ? this.state.data.map((a, i) => {
              return (
                <Step5Module
                  save={this.props.save}
                  key={i}
                  number={i}
                  data={a.data}
                  id={a.id}
                  add={(d) => this.addDataToElement(d)}
                  valorTerreno={this.state.area_total}
                  ceroIfNaN={this.props.ceroIfNaN}
                />)
            }
          ):[]}


          <div className="columns">
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Promedio (X)</label>
                <div className="field-body">
                  <div className="field ">
                    <p className="control is-expanded">
                      <input className="input is-small" value={String(this.state.promedio).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Mediana (Xmed)</label>
                <div className="field-body">
                  <div className="field ">
                    <p className="control is-expanded">
                      <input className="input is-small" value={String(this.state.mediana).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Desvest. M (S)</label>
                <div className="field-body">
                  <div className="field ">
                    <p className="control is-expanded">
                      <input className="input is-small" value={String(this.state.variacion).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-quarter">
              <div className="field">
                <label className="label is-small">Coef. de variación (CV)</label>
                <div className="field-body">
                  <div className="field ">
                    <p className="control is-expanded">
                      <input className="input is-small" value={this.state.coeficienteV} type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="divider"/>
          <h2 className="title is-4">
            Valor del terreno
          </h2>
          <div className="columns">
            <div className="column is-one-third">
              <div className="field">
                <label className="label is-small">Area total</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control is-expanded">
                      <input className="input is-small" name="area_total" value={this.state.area_total} onChange={this.handleChange} type="text" placeholder="" />
                    </p>
                    <p className="control">
                      <a className="button is-small is-static">
                        m²
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-third">
              <div className="field">
                <label className="label is-small">Valor unitario promedio en el sector</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input className="input is-small" value={String(this.state.mediana).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-one-third">
              <div className="field">
                <label className="label is-small">Valor total del terreno</label>
                <div className="field-body">
                  <div className="field has-addons">
                    <p className="control">
                      <a className="button is-small is-static">
                        RD$
                      </a>
                    </p>
                    <p className="control is-expanded">
                      <input name="valorTerreno" value={parseFloat(this.state.area_total * this.state.mediana).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} className="input is-small" type="text" placeholder="" />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }
}
