import React from 'react'
import { saveImg } from '../../../lib/utils'
import ImageEditor from '@toast-ui/react-image-editor'

export default class ImgEdit extends React.Component{
  constructor(props){
    super(props)

    this.toast = React.createRef()
    this.handleImg = this.handleImg.bind(this)
  }

  handleImg(){
    let refState = this.toast.current.getInstance()
    let data = refState.toDataURL()
    saveImg(data, this.props.path)

    console.log("from the img button save", this.props)
  }

  render(){
      // const ImageEditor = require('@toast-ui/react-image-editor')

      return(
        <>
        {ImageEditor ? (
          <>
          <ImageEditor
            ref={this.toast}
            includeUI={{
              loadImage: {
                path: 'file://' + this.props.path,
                name: 'SampleImage'
              },
              menu: ['filter', 'crop'],
              initMenu: 'filter',
              uiSize: {
                width: '1000px',
                height: '700px'
              },
              menuBarPosition: 'bottom'
            }}
            cssMaxHeight={500}
            cssMaxWidth={700}
            selectionStyle={{
              cornerSize: 20,
              rotatingPointOffset: 70
            }}
            usageStatistics={false}
          />
          <button onClick={this.handleImg}>Guardar</button>
          </>
        ):[]}
        </>
      )
  }
}
