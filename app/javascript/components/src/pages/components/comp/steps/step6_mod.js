import React from 'react'

export default class Step6Mod extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      key: '',
      value: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.save = this.save.bind(this)
  }

  async save(){
    await this.props.add({
      id: this.props.id,
      state: this.state
    })
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: value})
  }

  async componentDidMount(){
    console.log("DATA FROM PROPS MODULE STEP 6 MOD =---->", this.props.data)
    await this.setState(this.props.data)
  }

  render(){
    return (
      <div className="field is-grouped">
        <p className="control">
          <a className="button is-small is-static">
            {this.props.number}
          </a>
        </p>
        <div className="control">
          <input name="key" value={this.state.key} onChange={this.handleChange} className="input is-small" type="text" placeholder=""/>
        </div>
        <div className="control is-expanded">
          <input name="value" value={this.state.value} onChange={this.handleChange} className="input is-small" type="text" placeholder=" "/>
        </div>
        <div className="field is-grouped">
          <p className="control">
            <a className="button is-small is-danger" onClick={() => this.props.removeElement(this.props.id)}>
              <i className="fas fa-minus"/>
            </a>
            &nbsp;
            <a className="button is-small is-info" onClick={this.save}>
              <i className="fas fa-save"/>
            </a>
          </p>
        </div>
      </div>
    )
  }
}
