import React from 'react'

export default class Modal extends React.Component {
  constructor(props){
    super(props)
    this.print = this.print.bind(this)
    this.modalref = React.createRef()
    this.save = this.save.bind(this)
  }

  print(id){

  }

  save(){
    let refState = this.modalref.current
    this.props.onSave(refState)
  }

  render(){
    return(
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card" style={this.props.styles}>
          <header className="modal-card-head">
            {this.props.title ? (<p className="modal-card-title">{this.props.title}</p>):(<p className="modal-card-title"></p>)}

            {this.props.print ? (
              <p className="level-item">
                <button className="button is-info is-outlined" onClick={() => this.print(this.props.print)}>
                  <span className="icon is-small">
                    <i className="fas fa-print"></i>
                  </span>
                  <span>Imprimir</span>
                </button>
              </p>
            ):[]}

            <button className="delete is-medium" aria-label="close" onClick={this.props.onClose}></button>
          </header>
          <section className="modal-card-body">
            <this.props.Child ref={this.modalref} data={this.props.data}/>
          </section>
          <footer className="modal-card-foot">
            <div className="buttons is-right">
              {this.props.saveButtonTitle ? (<button className="button is-success" onClick={this.save}>{this.props.saveButtonTitle}</button>):[]}
              {this.props.cancelButtonTitle ? (<button className="button" onClick={this.props.onClose}>{this.props.cancelButtonTitle}</button>):[]}
            </div>
          </footer>
        </div>
      </div>
    )
  }
}
