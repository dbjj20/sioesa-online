import React from 'react'
import Body from './body'

export default class Complete extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(<Body props={this.props}/>)
  }
}
