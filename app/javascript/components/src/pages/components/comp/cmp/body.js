import ReactPaginate from 'react-paginate'
import Modal from '../modal'
import map from '../steps/map'
import React, { useState, useEffect } from 'react'

export default function Body ({props}){
	const [mapData, setMapData] = useState({})
	const [modal, startModal] = useState(false)
	
	function startMapView(d){
		if(d){
			let {latitud_norte, longitud_oeste, zoom} = d
			setMapData({latitud_norte, longitud_oeste, zoom})
			startModal(true)
		}
	}
	
	return(
		<>
			<div className="panel-block min-height">
				<table className="table is-fullwidth">
					<thead>
					<tr>
						<th>NO</th>
						<th>Cliente</th>
						<th>Dirección</th>
						<th>Fecha</th>
						<th>Estado</th>
						<th>Acciones</th>
					</tr>
					</thead>
					<tbody>
					{props.state.proyectos ? (
						props.state.proyectos.map((p, i) =>{
							let progress = `progress ${p.porcentaje < 30 ? 'is-danger': ''} ${p.porcentaje >= 30 && p.porcentaje < 95 ? 'is-warning' : 'is-success'}`
							return(
								<tr key={i}>
									<td><strong>{p.id}</strong></td>
									<td onClick={props.openModal}><a>{p.name}</a></td>
									<td>{p.direccion}</td>
									<td><time dateTime={`${p.fecha}`}>{p.fecha}</time></td>
									<td><progress className={progress} value={p.porcentaje} max="100">{p.porcentaje}%</progress></td>
									<td>
										<div>
											<button className="button is-small" onClick={() => props.changeFatherState({newTSC: true, proyecto_id: p.id})}>editar</button>
											<button className="button is-small" onClick={() => props.changeFatherState({newTSC: true, proyecto_id: p.id, print: true})}>ver</button>
											<button className="button is-small" onClick={() => startMapView(p.descripcion_inmueble[0])}>
                                  <span className="icon is-small">
                                    <i className="fas fa-map-marker-alt"/>
                                  </span>
											</button>
										</div>
									</td>
								</tr>
							)
						})
					):[]}
					</tbody>
				</table>
			</div>
			<div>
				<div className="">
					{props.state.proyectos && props.state.proyectos.length >= 1 ? (
						<nav className="pagination is-centered is-fullwidth">
							<a className='pagination-previous' onClick={() => props.calculatePageList({who: 'prev', selected: props.state.pageNumber - 1})}><span className="icon is-small">
                      <i className="fas fa-chevron-left"/>
                    </span>Anterior
							</a>
							<a className='pagination-next' onClick={() => props.calculatePageList({who: 'next', selected: props.state.pageNumber + 1})}>Próximo <span className="icon is-small">
                      <i className="fas fa-chevron-right"/>
                    </span>
							</a>
							<ReactPaginate
								previousLabel={''}
								nextLabel={''}
								breakLabel={'....'}
								pageCount={props.state.pagesCount} //pagesCount
								marginPagesDisplayed={4}
								pageRangeDisplayed={2}
								onPageChange={(e) => props.calculatePageList({who: 'comp', selected: e.selected})}
								activeLinkClassName={'is-current'}
								containerClassName={'pagination-list'}
								previousClassName={'is-hidden'}
								nextClassName={'is-hidden'}
								pageLinkClassName={'pagination-link'}
								previousLinkClassName={''}
								nextLinkClassName={''}
								forcePage={Number(props.state.pageNumber) - 1}
							/>
						</nav>
					):[]}
				</div>
				{modal ? (
					<Modal
						title="Ubicación"
						onClose={async () => { await startModal(!modal) }}
						data={{lat: mapData.latitud_norte, lng: mapData.longitud_oeste, zoom: mapData.zoom}}
						Child={map}
					/>):[]}
			</div>
		</>
	)
}
