import React from 'react'
import Steps from './steps/index'
import { getStore } from '../../lib/reduxito'
import Preview from './preview'
import Modal from './modal'
import Complete from './cmp/complete'
import InComplete from './cmp/incomplete'
import All from './cmp/all'
import moment from 'moment'
const date_format = "dddd D [de] MMMM yyyy"

export default class FirstView extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      newTSC: false,
      proyectos: [],
      pageNumber: 1,
      pagesCount: 0,
      isModalOpen: false,
      proyecto_id: undefined,
      print: false,
      limit: 10,
      inView: 'all',
      status: '',
      render: true
    }

    this.calculatePageList = this.calculatePageList.bind(this)
    this.search = this.search.bind(this)
    this.seePreview = this.seePreview.bind(this)
    this.changeView = this.changeView.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.cancel = this.cancel.bind(this)
    this.create = this.create.bind(this)
  }
  async create() {

    let date = moment().format()
    try {
      await getStore( async (store) => {
        console.log("PROYECTO MODULE DB =------>", store.db.proyecto)
        let pro
        if (this.props.proyecto_id){
          pro = await store.db.proyecto.where("id").equals(this.props.proyecto_id).toArray()
        }else {
          pro = await store.db.proyecto.where("name").equals("none").toArray()
        }
        // let pro = await store.db.proyecto.where("name").equals("none").toArray()
        console.log("PROYECTO PRO PRO =------>", pro)
        if (pro[0]){
          console.log(this.props.proyecto_id , pro[0].id)
          await this.setState({
            proyecto_id: pro[0].id, new: this.props.proyecto_id ? false : true,
           })

          console.log(this.state)
        }else {

          console.log("CREATING PROYECT")
          let result = await store.db.proyecto.add({
            name: 'none',
            fecha: moment().format(date_format),
            direccion: 'N/A',
            created_at: date,
            updated_at: date,
            project_name: '',
            searchable: '',
            status: 'incomplete',
            created_by_detail: ``, // use whoami using fs to get the username of the pc/mac
            updated_by_detail: `` // use whoami using fs to get the username of the pc/mac
          })
          await this.setState({proyecto_id: result, new: true})
          console.log(result, this.state)

        }
      })
    } catch (e) {
      console.log(e, e.message)
    }
  }

  async cancel() {
    await this.setState({render: false})
    // this.forceUpdate()
    // debugger
    await this.setState({render: true})
  }

  async handleSearch(e){
    if (e.target.value){
      await this.search({filter: true, status: this.state.status, searchable: `${e.target.value}`})
    }else {
      if (this.state.status){
        await this.search({status: this.state.status})
      }else {
        await this.search()
      }

    }
  }

  async changeView(view){
    await this.setState({pageNumber: 1})
    switch (view) {
      case 'all':
        await this.search()
        break;

      case 'complete':
        await this.search({status: view})
        break;

      case 'incomplete':
        await this.search({status: view})
        break;

      default:

    }
    await this.setState({inView: view, status: view === 'all' ? '' : view})
  }

  async seePreview(){
    await this.setState({isModalOpen: true})
  }

  async calculatePageList(d) {
    let go = true
    console.log(d, this.state.pageNumber)
    switch (d.who) {
      case 'comp':
        await this.setState({pageNumber: d.selected + 1})
        break;
      case 'next':
        if (this.state.pageNumber !== this.state.pagesCount){
          await this.setState({pageNumber: d.selected})
        } else {
          go = false
        }
        break;
      case 'prev':
        if(this.state.pageNumber !== 1){
          await this.setState({pageNumber: d.selected})
        } else {
          go = false
        }
        break;
      default:
    }
    if (go){
      console.log("SEARCHING", this.state)
      if (this.state.status){
        await this.search({status: this.state.status})
      }else {
        await this.search()
      }
    }
  }

  async search(query){
    await getStore(async (store) => {
      if (query && store){
        console.log('STORE IN SEARCH KSDKSDNLKSD', store)
        let offset = (Number(this.state.pageNumber) > 1 ?  (Number(this.state.limit) * Number(this.state.pageNumber - 1)) : 0)
        let CONST_PRO = []
        if (query.filter) {
          console.log('IN SEARCH INPUT ')

          let keys = ['by_pname', 'by_prop_name', 'by_cedula', 'by_mu_name', 'by_rnc', 'by_prov_name', 'id']
          let result
          for (let key of keys) {
            // let result = await store.db.proyecto.where(key).startsWithIgnoreCase(query.searchable).toArray()
            if (key === 'id'){
              result = await store.dataFetch({}, `/project/${query.searchable}`, 'post')
              CONST_PRO.push(result)
            }else {
              result = await store.dataFetch({}, `/search/${key}/${query.searchable}`, 'post')
            }

            debugger
            // for (let item of result) {
            //   if (key !== 'id'){
            //     CONST_PRO.push(item)
            //   }
            // }
          }
        }else {
          delete query.filter
          delete query.searchable
          CONST_PRO = await store.db.proyecto.where(query).offset(offset).limit(this.state.limit).reverse().toArray()
        }

        console.log('IN SEARCH FIRTS VIEW COMP ----> ', CONST_PRO)
        // delete query.status
        let NEW_CONST_PRO = []
        for(let proyecto of CONST_PRO){
          proyecto.descripcion_inmueble = await store.db.descripcion_inmueble.where('proyecto_id').equals(proyecto.id).toArray()
          NEW_CONST_PRO.push(proyecto)
        }

        await this.setState({
          proyectos: NEW_CONST_PRO,
          pagesCount: query.filter ? 1 : Math.ceil(await store.db.proyecto.where(query).count()/this.state.limit)
        })

      } else {
        let offset = (Number(this.state.pageNumber) > 1 ?  (Number(this.state.limit) * Number(this.state.pageNumber - 1)) : 0)
        // let CONST_PRO = await store.db.proyecto.toCollection().offset(offset).limit(this.state.limit).reverse().toArray()
        let CONST_PRO = await store.dataFetch({},`/all/projects/${this.state.limit}/${offset}`, 'post')

        console.log('IN SEARCH FIRTS VIEW COMP ----> ', CONST_PRO)
        await this.setState({
          proyectos: CONST_PRO.data,
          pagesCount: Math.ceil(CONST_PRO.count/this.state.limit)
        })
      }

    })
  }

  async componentDidMount(){
    await this.search()
  }

  render(){
    return(
      <>
        {this.state.newTSC ? (
          <div className="container">
              <Steps
                proyecto_id={this.state.proyecto_id ? this.state.proyecto_id : ''}
                print={this.state.print}
                cancel={() => this.props.cancel()}
              />
          </div>
        ) : (
          <div className="container">
              <div className="level">
                  <div className="level-left">
                      <div className="level-item">
                          <h1 className="title is-1">
                              Tasaciones
                          </h1>
                      </div>
                  </div>
                  <div className="level-right">
                      <p className="level-item">
                          <a className="button is-primary is-outlined" onClick={async () => await this.setState({newTSC: true})}>
                              <span className="icon is-small">
                                  <i className="fas fa-plus" aria-hidden="true"></i>
                              </span>
                              <span> Crear nueva tasación</span>
                          </a>
                      </p>
                  </div>
              </div>
              <article className="panel">
                  <div className="panel-block">
                      <p className="control has-icons-left">
                          <input className="input" type="text" placeholder="Buscar tasación" onChange={this.handleSearch}/>
                          <span className="icon is-left">
                              <i className="fas fa-search" aria-hidden="true"></i>
                          </span>
                      </p>
                  </div>
                  <p className="panel-tabs">
                      <a className={this.state.inView === 'all' ? "is-active" : ''} onClick={() => this.changeView('all')}>All</a>
                      <a className={this.state.inView === 'complete' ? "is-active" : ''} onClick={() => this.changeView('complete')}>Completadas</a>
                      <a className={this.state.inView === 'incomplete' ? "is-active" : ''} onClick={() => this.changeView('incomplete')}>Incompletas</a>
                  </p>
                  {this.state.inView === 'all' ? (<All state={this.state} calculatePageList={(a) => this.calculatePageList(a)} changeFatherState={async (d) => await this.setState(d)} />): []}
                  {this.state.inView === 'complete' ? (<Complete state={this.state} calculatePageList={(a) => this.calculatePageList(a)} changeFatherState={async (d) => await this.setState(d)}/>):[]}
                  {this.state.inView === 'incomplete' ? (<InComplete state={this.state} calculatePageList={(a) => this.calculatePageList(a)} changeFatherState={async (d) => await this.setState(d)}/>):[]}
              </article>
          </div>
        )}
      </>
    )
  }
}
