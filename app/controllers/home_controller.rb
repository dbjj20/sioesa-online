class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def add_pro
    @pro = current_user.projects.build(project_params)
     if @pro.save
       render json: {status: 200, ref: request.referrer, id: @pro.id}
     else
       render json: {status: 404, ref: request.referrer}
     end
  end

  def get_pro
    begin
      @data = Project.find(params[:id])
      render json: {status: 200, ref: request.referrer, data: @data}
    rescue
      render json: {status: 404, ref: request.referrer, data: [], message: 'error'}
    end
  end

  def update_pro
    @pro = Project.find(params[:id])
     if @pro.update(project_params)
       render json: {status: 200, ref: request.referrer}
     else
       render json: {status: 404, ref: request.referrer}
     end
  end

  def get_all
    @limit = params[:limit] || 10
    @offset = params[:offset] || 0
    @count = Project.count
    @data = Project.where.not(id: nil).limit(@limit).offset(@offset)
    render json: {status: 404, ref: request.referrer, data: @data, count: @count}
  end

  def search
    begin
      @pro = Project.where("#{params[:key]} ILIKE ?", "%" + params[:value] + "%")
      render json: {status: 200, ref: request.referrer, data: @pro}
    rescue
      render json: {status: 404, ref: request.referrer, data: []}
    end
  end

  private
  def project_params
    params.require(:home).permit(
      :name, :fecha, :direccion, :project_name, :status, :porcentage,
      :by_pname,
      :by_prop_name,
      :by_cedula,
      :by_mu_name,
      :by_rnc,
      :by_prov_name,
      :progres => {},
      :data => {})
  end
end
